package com.redhat.solutions.bpms;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;

import org.jbpm.services.task.identity.DBUserGroupCallbackImpl;
import org.jbpm.services.task.identity.DefaultUserInfo;
import org.jbpm.services.task.identity.JAASUserGroupCallbackImpl;
import org.jbpm.kie.services.cdi.producer.UserGroupInfoProducer;
import org.jbpm.shared.services.cdi.Selectable;
import org.kie.api.task.UserGroupCallback;
import org.kie.internal.task.api.UserInfo;

@ApplicationScoped
@Alternative
@Selectable
public class CustomUserGroupProducer implements UserGroupInfoProducer {

	private UserGroupCallback callback = null;
	private UserInfo userInfo = null;

	@Override
	@Produces
	public UserGroupCallback produceCallback() {
		if(callback == null) {
			String callbackType = System.getProperty("jbpm.usergroup.callback.type", "jaas");
			
			if("jaas".equals(callbackType)) {
				callback = new JAASUserGroupCallbackImpl(true);
			} else if("db".equals(callbackType)) {
				callback = new DBUserGroupCallbackImpl(true);
			} else if("custom".equals(callbackType)) {
				callback = new CustomUserGroupCallbackImpl(true);
			}
		}
		
		return callback;
	}

	@Override
	@Produces
	public UserInfo produceUserInfo() {
		if(userInfo == null) {
			String callbackType = System.getProperty("jbpm.userinfo.type", "default");
			
			if("default".equals(callbackType)) {
				userInfo = new DefaultUserInfo(true);
			}
		}
		return userInfo;
	}
	
}
