select sid, serial#, username, 'alter system kill session ''' || sid || ',' || serial# || ''';' from gv$session where REGEXP_LIKE(username,'jbpmapp','i');

-- Forcefully disconnect the users RVR and RDW
BEGIN
   FOR ln_cur IN (SELECT sid, serial# FROM v$session WHERE REGEXP_LIKE(username,'jbpmapp','i'))
   LOOP
      EXECUTE IMMEDIATE ('ALTER SYSTEM KILL SESSION ''' || ln_cur.sid || ',' || ln_cur.serial# || ''' IMMEDIATE');
   END LOOP;
END;
/

DROP USER jbpmapp CASCADE;
CREATE USER jbpmapp IDENTIFIED BY jbpmapp;
GRANT CREATE SESSION TO jbpmapp;
GRANT CONNECT TO jbpmapp;
GRANT RESOURCE TO jbpmapp;

GRANT SELECT ON sys.dba_pending_transactions TO jbpmapp;
GRANT SELECT ON sys.pending_trans$ TO jbpmapp;
GRANT SELECT ON sys.dba_2pc_pending TO jbpmapp;
GRANT EXECUTE ON sys.dbms_xa TO jbpmapp;

