# TODO

1) Complete persistence of Approval domain object
2) Translate approval JPA to REST domain
3) Create Process Generation logic
  * Create process view page
  * Create process deploy logic
  * Research how to do human task form for approval?
4) Update runtime manager to use generated approval (execute remotely?  embed in app?)
  * May have to create DB user group provide for business-central
  * Could mix deploy for remove editor view only
  * Load processed from the file system?
5) Update Approvals list page
6) Create User-Management page



# Approval Data Model

Approval
- name
- description
- created timestamp
- updated timestamp
- steps
  - count
  - all | any
  - approvers: user | group
- status: New, Created, Deployed
- process id
- version

# User Interface Design

> Approvals
  - List all existing approvals
  - Button to create new
> New Approval
  - Fields for name / description(MD)
  - Set Step Count Entry Box
    - Slider for Any|All
    - Approval Count Entry Box
      - User/Group Selection Box for each approval
  - Button for generate workflow
  - Button for deploy workflow once generated
> View Approval
  - Display details
  - IFrame of BPMN2 workflow
  - Button for deploy workflow

# REST Services

> Approval
  - GET - returns all approvals
  - :id GET - return requested approval
  - :id PUT - returns saved aproval
  - :id DELETE - deletes both project and approval 
  - :id/generate POST  - returns info about project generated
  - :id/deploy POST - returns deployment success and additional info
> UserMgmt
  - GET ALL

