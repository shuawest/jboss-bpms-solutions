

JTA + SharedEM
* You need to create and configure both an EMF and EM to ensure one entity manager is used by the various Java beans: ksession, taskService, & auditLogService.



Best Practices

a) Don't use the kruntime and AuditLog service within the same transaction.  Any saves from the runtime may not have been flushed to the database yet. 
b) Good description of JPA/JTA concepts http://tomee.apache.org/jpa-concepts.html 

# java.lang.IllegalStateException: This persistence strategy only deals with UserTransaction instances!

Added test to reproduce issue.  Pull request: https://github.com/droolsjbpm/droolsjbpm-integration/pull/89
~~~~~~~~~~~~~~~~~~~~~
The issue appears to be caused by the lack of a null check, before checking the transaction object type in `org.jbpm.process.audit.strategy.StandaloneJtaStrategy`. 

When using a shared entity manager and JTA transaction, each service action should not be interacting with the user transaction object (ex: begin/commit). Modifying the type check corrects the issue.

More thorough testing of Spring+JTA+SharedEM is advised. 

[1] https://github.com/shuawest/jbpm/commit/827200856b6f8e2568a6e14b9ffc5802383885f6
~~~~~~~~~~~~~~~~~~~~~

Description of problem:

When using Spring, JTA, and a Shared Entity Manager the following exception [4] is thrown when trying to use the audit log service is used. 

Looking into the SpringStandaloneJtaSharedEntityManagerStrategy implementation [1], the illegal state is thrown by a null transaction [2], resulting from the joinTransaction method only returning new transactions [3].

There are currently no tests for the Spring-JTA-SharedEM strategy. 


Version-Release number of selected component (if applicable):
* Found in BPMS 6.0.2GA
* Reproduced in community JBPM 6.2.0-SNAPSHOT

How reproducible:

100%

Steps to Reproduce:
1. Start a transaction - a single transaction typically surrounds a serivce call
2. Create a process instance
3. Attempt to query the process instance from the audit log service

Actual results:

Exception thrown on use of the auditLogService.

Expected results:

AuditLogService usable within the same transaction as the task and runtime service.

Additional info:

[1] https://github.com/droolsjbpm/jbpm/blob/master/jbpm-audit/src/main/java/org/jbpm/process/audit/strategy/SpringStandaloneJtaSharedEntityManagerStrategy.java
[2] https://github.com/droolsjbpm/jbpm/blob/master/jbpm-audit/src/main/java/org/jbpm/process/audit/strategy/StandaloneJtaStrategy.java#L97
[3] https://github.com/droolsjbpm/jbpm/blob/master/jbpm-audit/src/main/java/org/jbpm/process/audit/strategy/StandaloneJtaStrategy.java#L59
[4] java.lang.IllegalStateException: This persistence strategy only deals with UserTransaction instances!
	at org.jbpm.process.audit.strategy.StandaloneJtaStrategy.commitTransaction(StandaloneJtaStrategy.java:98)
	at org.jbpm.process.audit.strategy.SpringStandaloneJtaSharedEntityManagerStrategy.leaveTransaction(SpringStandaloneJtaSharedEntityManagerStrategy.java:29)
	at org.jbpm.process.audit.JPAAuditLogService.closeEntityManager(JPAAuditLogService.java:335)
	at org.jbpm.process.audit.JPAAuditLogService.findProcessInstance(JPAAuditLogService.java:164)
	at org.kie.spring.jbpm.JTASharedEntityManagerFactorySpringTest.testSpringWithJTAAndSharedEMF(JTASharedEntityManagerFactorySpringTest.java:48)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:606)
	at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:47)
	at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)
	at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:44)
	at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)
	at org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)
	at org.junit.internal.runners.statements.RunAfters.evaluate(RunAfters.java:27)
	at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:271)
	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:70)
	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:50)
	at org.junit.runners.ParentRunner$3.run(ParentRunner.java:238)
	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:63)
	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:236)
	at org.junit.runners.ParentRunner.access$000(ParentRunner.java:53)
	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:229)
	at org.junit.runners.ParentRunner.run(ParentRunner.java:309)
	at org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:50)
	at org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:38)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:467)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:683)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:390)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:197)



# Spring/JBPM: IllegalStateException: RuntimeManager with id spring-rmr is already active

https://bugzilla.redhat.com/show_bug.cgi?id=1121764

#  "This session was previously disposed" when used Spring+JTA+PER_PROCESS_INSTANCE

https://bugzilla.redhat.com/show_bug.cgi?id=1121181

Now happening with PER_REQUEST mode


