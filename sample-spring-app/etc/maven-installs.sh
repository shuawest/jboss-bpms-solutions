#!/bin/sh

mvn install:install-file -Dfile="./oracle/main/ojdbc6.jar" -DgroupId=com.oracle -DartifactId=ojdbc -Dversion=6 -Dpackaging=jar

mvn install:install-file -Dfile="./mysql/main/mysql-connector-java.jar" -DgroupId=com.mysql -DartifactId=mysql-connector-java -Dversion=5.5 -Dpackaging=jar

