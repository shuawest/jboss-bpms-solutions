package com.redhat.solutions.bpms;

import java.io.Serializable;
import java.util.List;

public class AppStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	private String appVersion;
	private String buildDate;
	
	private String databaseStatus;
	private List<String> databaseEntities;
	private List<String> databaseProperties;

	private String jbpmStatus;

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}

	public String getDatabaseStatus() {
		return databaseStatus;
	}

	public void setDatabaseStatus(String databaseStatus) {
		this.databaseStatus = databaseStatus;
	}

	public List<String> getDatabaseEntities() {
		return databaseEntities;
	}

	public void setDatabaseEntities(List<String> databaseEntities) {
		this.databaseEntities = databaseEntities;
	}

	public List<String> getDatabaseProperties() {
		return databaseProperties;
	}

	public void setDatabaseProperties(List<String> databaseProperties) {
		this.databaseProperties = databaseProperties;
	}

	public String getJbpmStatus() {
		return jbpmStatus;
	}

	public void setJbpmStatus(String jbpmStatus) {
		this.jbpmStatus = jbpmStatus;
	}

}