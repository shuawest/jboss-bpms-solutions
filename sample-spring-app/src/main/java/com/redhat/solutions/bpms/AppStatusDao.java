package com.redhat.solutions.bpms;


public interface AppStatusDao
{
    AppStatus getAppStatus();
}
