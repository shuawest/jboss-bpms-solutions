package com.redhat.solutions.bpms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.metamodel.ManagedType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class AppStatusDaoImpl implements AppStatusDao {

	@PersistenceContext
	private EntityManager em;

	@Value("${application.version}")
	private String appVersion;

	@Value("${application.build.date}")
	private String appBuildDate;
	
	public AppStatus getAppStatus() {
		AppStatus status = new AppStatus();
		status.setAppVersion(appVersion);
		status.setBuildDate(appBuildDate);
		
		status.setDatabaseStatus((isConnected()? "Connected" : "Disconnected"));
		status.setDatabaseEntities(getDatabaseEntities());
		status.setDatabaseProperties(getDatabaseProperties());
		
		status.setJbpmStatus("JBPM Not Used Yet");
		
		return status;
	}

	private boolean isConnected() {
		return em.isOpen();
	}
	
	private List<String> getDatabaseEntities() {
		List<String> entities = new ArrayList<String>();
		try {
			Set<ManagedType<?>> types = em.getMetamodel().getManagedTypes();

			for (ManagedType<?> entry : types) {
				entities.add(entry.getJavaType().getName());
			}
		} catch (Exception e) {
			entities.add("DB Error: " + e.getMessage());
		}
		return entities;
	}
	
	private  List<String> getDatabaseProperties() {
		List<String> dbprops = new ArrayList<String>();
		try {
			Map<String, Object> emfProperties = em.getEntityManagerFactory().getProperties();

			for (Entry<String, Object> entry : emfProperties.entrySet()) {
				dbprops.add(entry.getKey() + " = " + entry.getValue());
			}
		} catch (Exception e) {
			dbprops.add("DB Error: " + e.getMessage());
		}
		return dbprops;
	}
}
