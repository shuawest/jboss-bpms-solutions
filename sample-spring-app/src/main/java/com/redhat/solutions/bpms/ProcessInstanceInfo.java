package com.redhat.solutions.bpms;

import java.io.Serializable;

public class ProcessInstanceInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String processId;
	private String processName;
	private int state;
	private String stateDesc;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getStateDesc() {
		return stateDesc;
	}
	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

}
