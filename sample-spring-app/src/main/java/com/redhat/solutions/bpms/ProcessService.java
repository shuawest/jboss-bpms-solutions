package com.redhat.solutions.bpms;

import java.util.List;

public interface ProcessService {

    List<ProcessInfo> findAllProcesses();
    
    List<ProcessInstanceInfo> findAllProcessInstances();
    
    ProcessInstanceInfo startProcessInstance();
}
