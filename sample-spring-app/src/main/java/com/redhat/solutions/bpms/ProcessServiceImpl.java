package com.redhat.solutions.bpms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jbpm.process.audit.AuditLogService;
import org.jbpm.process.audit.ProcessInstanceLog;
import org.kie.api.definition.process.Process;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ProcessServiceImpl implements ProcessService {

	private static Logger logger = LoggerFactory.getLogger(ProcessServiceImpl.class);
	
	@Autowired
    private RuntimeManager runtimeManager;

	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	private TaskService taskService;
	
    public List<ProcessInfo> findAllProcesses()
    {
    	RuntimeEngine engine = runtimeManager.getRuntimeEngine(null);
    	KieSession session = engine.getKieSession();
    	Collection<Process> processes = session.getKieBase().getProcesses();

    	return convertProcesses(processes);
    }
    
    public List<ProcessInstanceInfo> findAllProcessInstances()
    {
    	List<ProcessInstanceLog> instances = auditLogService.findProcessInstances();
    	
    	return convertInstances(instances);
    }
    
    public ProcessInstanceInfo startProcessInstance()
    {
    	RuntimeEngine engine = runtimeManager.getRuntimeEngine(null);
    	KieSession ksession = engine.getKieSession();
    	ProcessInstance processInstance = ksession.startProcess("com.redhat.solutions.bpms.sample2", null);
    	return convertInstance(processInstance);
    }
     
    private ProcessInfo convertProcess(Process process) {
    	ProcessInfo info = new ProcessInfo();	
    	info.setId(process.getId());
    	info.setName(process.getName());
    	info.setNamespace(process.getNamespace());
    	info.setPackageName(process.getPackageName());
    	info.setVersion(process.getVersion());
    	return info;
    }
    
    private List<ProcessInfo> convertProcesses(Collection<Process> processes) {
    	List<ProcessInfo> infos = new ArrayList<ProcessInfo>();
    	for(Process process : processes) {
    		infos.add(convertProcess(process));
    	}
    	return infos;
    }

    private ProcessInstanceInfo convertInstance(ProcessInstanceLog instance) {
    	ProcessInstanceInfo info = new ProcessInstanceInfo();
    	info.setId(instance.getProcessInstanceId());
    	info.setProcessId(instance.getProcessId());
    	info.setProcessName(instance.getProcessName());
    	info.setState(instance.getStatus());
    	info.setStateDesc(convertStatusToDesc(instance.getStatus()));
        return info;
    }
    
    private ProcessInstanceInfo convertInstance(ProcessInstance instance) {
    	ProcessInstanceInfo info = new ProcessInstanceInfo();
    	info.setId(instance.getId());
    	info.setProcessId(instance.getProcessId());
    	info.setProcessName(instance.getProcessName());
    	info.setState(instance.getState());
    	info.setStateDesc(convertStatusToDesc(instance.getState()));
        return info;
    }
    
    private String convertStatusToDesc(int state) {
    	String stateDesc;
    	switch (state) {
    	case 0:
    		stateDesc = "STATE_PENDING";
    		break;
    	case 1:
    		stateDesc = "STATE_ACTIVE";
    		break;
    	case 2:
    		stateDesc = "STATE_COMPLETED";
    		break;
    	case 3:
    		stateDesc = "STATE_ABORTED";
    		break;
    	case 4:
    		stateDesc = "STATE_SUSPENDED";
    		break;
		default:
			stateDesc = "UNDEFINED";
			break;
		}
    	return stateDesc;
    }

    private List<ProcessInstanceInfo> convertInstances(List<ProcessInstanceLog> instances) {
    	List<ProcessInstanceInfo> infos = new ArrayList<ProcessInstanceInfo>();
    	for(ProcessInstanceLog instance : instances) {
    		infos.add(convertInstance(instance));
    	}
    	return infos;
    }

}
