package com.redhat.solutions.bpms.approval;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Approval implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String STATUS_NEW = "new";
	public static final String STATUS_READY = "ready";
	public static final String STATUS_GENERATED = "generated";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String name;
	private String description;
	private Date created;
	private Date updated;
	private String status;
	private String processId;
	private String version = "1.0";
	
	@OneToMany(mappedBy="approval", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<ApprovalStep> steps = new ArrayList<ApprovalStep>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<ApprovalStep> getSteps() {
		return steps;
	}

	public void setSteps(List<ApprovalStep> steps) {
		this.steps = steps;
	}
	
	public void addStep(ApprovalStep step) {
		step.setApproval(this);
		this.steps.add(step);
	}
	
	public void removeStep(ApprovalStep step) {
		step.setApproval(null);
		this.steps.remove(step);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Approval other = (Approval) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Approval [id=" + id + ", name=" + name + ", description=" + description + ", created=" + created + ", updated=" + updated + ", status=" + status + ", processId="
				+ processId + ", version=" + version + "]";
	}
	
}
