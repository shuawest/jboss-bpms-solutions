package com.redhat.solutions.bpms.approval;

import java.util.List;

public interface ApprovalDao {

	Approval findById(long id);
	
	List<Approval> findAll();	
	
	Approval saveApproval(Approval approval);
	
	void removeApproval(Long approvalId);
}
