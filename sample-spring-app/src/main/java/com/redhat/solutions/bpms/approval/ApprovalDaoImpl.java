package com.redhat.solutions.bpms.approval;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.redhat.solutions.bpms.usermgmt.TaskGroupAssignment;
import com.redhat.solutions.bpms.usermgmt.UserManagementServiceImpl;

@Repository
@Transactional
public class ApprovalDaoImpl implements ApprovalDao {

	private static final Logger logger = LoggerFactory.getLogger(ApprovalDaoImpl.class);

	@PersistenceContext
	private EntityManager em;

	public Approval findById(long id) {
		Approval approval = em.find(Approval.class, id);
		preloadGraph(approval);
		return approval;
	}

	public List<Approval> findAll() {
		TypedQuery<Approval> query = em.createQuery("SELECT a FROM Approval a ORDER BY a.name", Approval.class);
		List<Approval> approvals = query.getResultList();
		
		for(Approval approval : approvals)
			preloadGraph(approval);
		
        return approvals;
	}
	
	private void preloadGraph(Approval approval) {
		if(approval != null) {
			Hibernate.initialize(approval.getSteps());
			logger.debug("Initialized steps: {}", approval.getSteps());
			for(ApprovalStep step : approval.getSteps()) {
				Hibernate.initialize(step.getApprovers());
				logger.debug("Initialized approvers {}", step.getApprovers().size());
			}
		}
	}

	public Approval saveApproval(Approval approval) {
		return em.merge(approval);
	}
	
	public void removeApproval(Long approvalId) {
		Approval approval = findById(approvalId);
		em.remove(approval);
	}
}
