package com.redhat.solutions.bpms.approval;

import java.util.Stack;

import org.apache.commons.io.IOUtils;
import org.jbpm.bpmn2.xml.XmlBPMNProcessDumper;
import org.jbpm.ruleflow.core.RuleFlowProcess;
import org.jbpm.ruleflow.core.RuleFlowProcessFactory;
import org.jbpm.workflow.core.node.Join;
import org.jbpm.workflow.core.node.Split;
import org.kie.api.io.Resource;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApprovalProcessBuilder {
	private final Logger logger = LoggerFactory.getLogger(ApprovalProcessBuilder.class);

	public static final int GATEWAY_Y = 126;
	public static final int TASK_Y = 110;
	public static final int TASK_Y_OFFSET = 150;
	public static final int X_OFFSET = 200;
	public static final int TASK_WIDTH = 130;
	public static final int TASK_HEIGHT = 80;

	private RuleFlowProcessFactory factory;
	private Approval approval;

	private String packageName;
	private String processName;
	private String fullProcessName;
	private RuleFlowProcess process;
	private String processXml;

	private Long nodeId = 0L;
	private Stack<NodeInfo> headNodes = new Stack<NodeInfo>();
	private Stack<NodeInfo> nodes = new Stack<NodeInfo>();

	public ApprovalProcessBuilder(Approval approval, String packageName) {
		this.approval = approval;
		this.packageName = packageName;
	}

	public void buildProcess() {
		buildBaseProcess();

		int stepIndex = 0;
		for (ApprovalStep step : approval.getSteps()) {
			stepIndex++;
			if (step.getApprovers().size() == 1) {
				buildSingleApprovalStep(step, stepIndex);
			} else {
				buildApprovalStep(step, stepIndex);
			}
		}

		buildEndNode();

		process = factory.validate().getProcess();

		layoutProcess();

		convertToXml();
	}
	
	public String getProcessXml() {
		return processXml;
	}
	public RuleFlowProcess getProcess() {
		return process;
	}
	
	
	
	// struct for storing node position data during construction
	class NodeInfo {
		Long id;
		int x;
		int y;
		int height = 48;
		int width = 48;
	}

	protected NodeInfo newNode() {
		NodeInfo node = new NodeInfo();
		node.id = ++nodeId;
		nodes.push(node);
		return node;
	}

	protected NodeInfo newHeadNode() {
		NodeInfo node = newNode();
		headNodes.push(node);
		return node;
	}

	protected NodeInfo currentNode() {
		return nodes.peek();
	}

	protected NodeInfo currentHeadNode() {
		return headNodes.peek();
	}

	protected void buildBaseProcess() {
		NodeInfo node = newHeadNode();
		node.x = 30;
		node.y = GATEWAY_Y;
		
		processName = approval.getName().replaceAll("\\s+", "");
		fullProcessName = packageName + "." + processName;

		factory = RuleFlowProcessFactory.createProcess(fullProcessName);
		factory.name(processName).packageName(packageName).startNode(node.id).name("Start").done();
	}

	protected void buildSingleApprovalStep(ApprovalStep step, int stepIndex) {
		NodeInfo prevNode = currentHeadNode();
		NodeInfo node = newHeadNode();
		node.x = prevNode.x + X_OFFSET;
		node.y = TASK_Y;
		node.height = TASK_HEIGHT;
		node.width = TASK_WIDTH;

		StepApprover approver = step.getApprovers().get(0);
		String nodeName = String.format("Step %d %s", stepIndex, approver.getLabel());

		factory.humanTaskNode(node.id).name(nodeName).taskName(nodeName).actorId(approver.getPrincipalId()).done();
		factory.connection(prevNode.id, node.id);
	}

	protected void buildApprovalStep(ApprovalStep step, int stepIndex) {
		NodeInfo prevNode = currentHeadNode();

		// Split Node
		NodeInfo splitNode = newHeadNode();
		splitNode.x = prevNode.x + X_OFFSET;
		splitNode.y = GATEWAY_Y;
		String splitType = (step.getType().equals(ApprovalStep.ALL_APPROVE)) ? "All must approve" : "Any may approve";
		String splitName = String.format("Step %d %s", stepIndex, splitType);
		factory.splitNode(splitNode.id).type(Split.TYPE_AND).name(splitName).done();
		factory.connection(prevNode.id, splitNode.id);

		// Join Node
		NodeInfo joinNode = newHeadNode();
		joinNode.x = prevNode.x + X_OFFSET * 3;
		joinNode.y = GATEWAY_Y;
		if (step.getType().equals(ApprovalStep.ALL_APPROVE)) { // All approve
			factory.joinNode(joinNode.id).type(Join.TYPE_AND).done();
		} else { // Any Approve
			factory.joinNode(joinNode.id).type(Join.TYPE_OR).done();
		}

		// Approval Tasks
		int approvalTaskY = TASK_Y;
		for (StepApprover approver : step.getApprovers()) {
			NodeInfo approvalNode = newNode();
			approvalNode.x = prevNode.x + X_OFFSET * 2;
			approvalNode.y = approvalTaskY;
			approvalNode.height = TASK_HEIGHT;
			approvalNode.width = TASK_WIDTH;

			String nodeName = String.format("Step %d %s", stepIndex, approver.getLabel());
			factory.humanTaskNode(approvalNode.id).name(nodeName).taskName(nodeName).actorId(approver.getPrincipalId()).done();
			factory.connection(splitNode.id, approvalNode.id);
			factory.connection(approvalNode.id, joinNode.id);

			approvalTaskY += TASK_Y_OFFSET;
		}
	}

	protected void buildEndNode() {
		NodeInfo prevNode = currentHeadNode();
		NodeInfo node = newHeadNode();
		node.x = prevNode.x + X_OFFSET;
		node.y = GATEWAY_Y;
		factory.endNode(node.id).name("End").done();
		factory.connection(prevNode.id, node.id);
	}

	protected void layoutProcess() {
		for(NodeInfo node : nodes) {
			process.getNode(node.id).getMetaData().put("x", node.x);
			process.getNode(node.id).getMetaData().put("y", node.y);
			process.getNode(node.id).getMetaData().put("width", node.width);
			process.getNode(node.id).getMetaData().put("height", node.height);
		}
	}
	
	protected void convertToXml() {
		Resource res = ResourceFactory.newByteArrayResource(XmlBPMNProcessDumper.INSTANCE.dump(process, XmlBPMNProcessDumper.META_DATA_USING_DI).getBytes());
		res.setSourcePath("/tmp/processFactory.bpmn2");

		try {
			processXml = IOUtils.toString(res.getReader());
		} catch (Exception e) {
			logger.error("Error converting process to XML: ", e);
		}
	}
}
