package com.redhat.solutions.bpms.approval;

public interface ApprovalService {
	
	String generateProcessDefinition(Long approvalId);

}
