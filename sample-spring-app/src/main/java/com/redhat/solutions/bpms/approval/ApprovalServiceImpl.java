package com.redhat.solutions.bpms.approval;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ApprovalServiceImpl implements ApprovalService {
	private final Logger logger = LoggerFactory.getLogger(ApprovalServiceImpl.class);

	@Autowired
	private ApprovalDao approvalDao;
	
	@Value("${processGenerator.packageName}")
	private String packageName;
	@Value("${processGenerator.orgUnit}")
	private String orgUnit;
	@Value("${processGenerator.repository}")
	private String repository;
	
//	@Value("${jboss.bpms.repo.git.uri}")
//	private String repoGitUri;
//	@Value("${jboss.bpms.repo.git.user}")
//	private String repoGitUser;
//	@Value("${jboss.bpms.repo.git.pass}")
//	private String repoGitPass;
//	
//	@Value("${jboss.bpms.rest.uri}")
//	private String restBaseUri;
//	@Value("${jboss.bpms.rest.user}")
//	private String restUser;
//	@Value("${jboss.bpms.rest.pass}")
//	private String restPass;	

	
	public String generateProcessDefinition(Long approvalId) {
		Approval approval = approvalDao.findById(approvalId);
		
		ApprovalProcessBuilder procBuilder = new ApprovalProcessBuilder(approval, packageName);
		procBuilder.buildProcess();
		
		return procBuilder.getProcessXml();
	}
	
	public boolean createProject(String projectName) {
		return true;
	}
	
	public void deployProcessDefinition(String xml) {
		// verify or create Org Unit, Repository, and Project
		//    REST API to create Org Unit
		//    REST API to create Repository
		//    REST API to create project
		// git clone the project
		// add the bpmn2 file
		// git commit and push the file update
	}
	
	// http://stackoverflow.com/questions/17494996/simple-git-client-using-jgit-has-trouble-with-fetch-pull?rq=1
	// https://github.com/centic9/jgit-cookbook
	
//	public void cloneRepository(Long approvalId) throws InvalidRemoteException, TransportException, GitAPIException, IOException {
//		File localPath = File.createTempFile("tempApprovalRepo" + approvalId, "");
//        localPath.delete();
//
//        String gitLocal = "git://system";
//        String systemGitRepoUrl = "ssh://localhost:8001/system";
//        
//        URI gitUri = URI.create(systemGitRepoUrl);
//        if (gitUri.getScheme().equalsIgnoreCase("ssh")) {
//            systemGitRepoUrl = gitUri.getScheme()+"://"+repoGitUser+"@"+gitUri.getHost()+":"+ gitUri.getPort()+gitUri.getPath();
//        }
//        if (gitUri.getScheme().equalsIgnoreCase("ssh")) {
//            // use special credential provider to support prompt for SSH connections to unknown hosts
//            CredentialsProvider.setDefault(new UsernamePasswordCredentialsProvider(repoGitUser, repoGitPass));
//        }// else {
////            env.put("username", System.getProperty("git.user", user));
////            env.put("password", System.getProperty("git.password", password));
////        }
//
//        System.out.println("Cloning from " + systemGitRepoUrl + " to " + localPath);
//        Git.cloneRepository().setURI(systemGitRepoUrl).setDirectory(localPath).call();
//
//        FileRepositoryBuilder builder = new FileRepositoryBuilder();
//        Repository repository = builder.setGitDir(localPath).readEnvironment().findGitDir().build();
//
//        System.out.println("Having repository: " + repository.getDirectory());
//
//        repository.close();
//	}
	
}
