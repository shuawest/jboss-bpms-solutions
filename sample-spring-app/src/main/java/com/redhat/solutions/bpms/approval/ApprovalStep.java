package com.redhat.solutions.bpms.approval;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class ApprovalStep implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String ALL_APPROVE = "all";
	public static final String ANY_APPROVE = "any";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String type;
	
	@ManyToOne
	private Approval approval;

	@OneToMany(mappedBy="approvalStep", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<StepApprover> approvers = new ArrayList<StepApprover>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Approval getApproval() {
		return approval;
	}

	public void setApproval(Approval approval) {
		this.approval = approval;
	}

	public List<StepApprover> getApprovers() {
		return approvers;
	}

	public void setApprovers(List<StepApprover> approvers) {
		this.approvers = approvers;
	}
	
	public void addApprover(StepApprover approver) {
		approver.setApprovalStep(this);
		this.approvers.add(approver);
	}
	
	public void removeApprover(StepApprover approver) {
		approver.setApprovalStep(null);
		this.approvers.remove(approver);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApprovalStep other = (ApprovalStep) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ApprovalStep [id=" + id + ", type=" + type + "]";
	}

}
