package com.redhat.solutions.bpms.approval;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class StepApprover implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String USER_TYPE = "user";
	public static final String GROUP_TYPE = "group";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String principalId;
	private String type;
	private String label;
	
	@ManyToOne
	private ApprovalStep approvalStep;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPrincipalId() {
		return principalId;
	}
	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public ApprovalStep getApprovalStep() {
		return approvalStep;
	}
	public void setApprovalStep(ApprovalStep approvalStep) {
		this.approvalStep = approvalStep;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StepApprover other = (StepApprover) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "StepApprover [id=" + id + ", principalId=" + principalId + ", type=" + type + ", label=" + label + "]";
	}

}
