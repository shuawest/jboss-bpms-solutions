package com.redhat.solutions.bpms.approval.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.redhat.solutions.bpms.approval.ApprovalStep;

public class ApprovalRest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;	
	private String name;
	private String description;
	private Date created;
	private Date updated;
	private String status;
	private String processId;
	private String version;
	private ArrayList<ApprovalStepRest> steps = new ArrayList<ApprovalStepRest>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public ArrayList<ApprovalStepRest> getSteps() {
		return steps;
	}
	public void setSteps(ArrayList<ApprovalStepRest> steps) {
		this.steps = steps;
	}
	
	@Override
	public String toString() {
		return "ApprovalRest [id=" + id + ", name=" + name + ", description=" + description + ", created=" + created + ", updated=" + updated + ", status=" + status
				+ ", processId=" + processId + ", version=" + version + "]";
	}
		
}
