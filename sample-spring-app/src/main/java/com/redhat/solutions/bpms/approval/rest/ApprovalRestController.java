package com.redhat.solutions.bpms.approval.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.redhat.solutions.bpms.approval.Approval;
import com.redhat.solutions.bpms.approval.ApprovalDao;
import com.redhat.solutions.bpms.approval.ApprovalService;
import com.redhat.solutions.bpms.approval.ApprovalStep;
import com.redhat.solutions.bpms.approval.StepApprover;
import com.redhat.solutions.bpms.usermgmt.TaskGroup;
import com.redhat.solutions.bpms.usermgmt.TaskUser;
import com.redhat.solutions.bpms.usermgmt.UserManagementService;

@Controller
@RequestMapping("/rest/approvals")
public class ApprovalRestController {
	private final Logger logger = LoggerFactory.getLogger(ApprovalRestController.class);

    @Autowired
    private ApprovalDao approvalDao;
    
    @Autowired
    private ApprovalService approvalService;
    
    @Autowired 
    private UserManagementService userMgmt;
   
    
    @RequestMapping(value="", method=RequestMethod.GET, produces="application/json")
    public @ResponseBody List<ApprovalRest> listAllApprovals()
    {
    	List<ApprovalRest> ret = new ArrayList<ApprovalRest>();
    	for(Approval approval : approvalDao.findAll())
    		ret.add(convertApprovalToRest(approval));
        return ret;
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
    public @ResponseBody ApprovalRest findById(@PathVariable("id") Long id)
    {
        return convertApprovalToRest(approvalDao.findById(id));
    }

    @RequestMapping(value="", method=RequestMethod.POST, produces="application/json")
    public @ResponseBody ApprovalRest createApproval(@RequestBody ApprovalRest restApproval)
    {
    	// Create the new approval
    	Approval jpaApproval = new Approval();
    	jpaApproval.setCreated(new Date());
    	jpaApproval.setUpdated(new Date());
    	jpaApproval.setStatus(Approval.STATUS_READY);
    	
    	// Merge it with the rest data
    	Approval mergedApproval = mergeApproval(jpaApproval, restApproval);
    	
    	// Save it
    	mergedApproval = approvalDao.saveApproval(mergedApproval);
    	
    	// Convert the saved approval to rest
    	return convertApprovalToRest(mergedApproval);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT, produces="application/json")
    public @ResponseBody ApprovalRest updateApproval(@PathVariable("id") Long id, @RequestBody ApprovalRest restApproval)
    {
    	Approval jpaApproval = approvalDao.findById(id);
    	jpaApproval.setUpdated(new Date());
    	if(jpaApproval.getProcessId() == null)
    		jpaApproval.setStatus(Approval.STATUS_READY);

    	// Merge it with the rest data
    	Approval mergedApproval = mergeApproval(jpaApproval, restApproval);
    	
    	// Save it
    	mergedApproval = approvalDao.saveApproval(mergedApproval);
    	
    	// Convert the saved approval to rest
    	return convertApprovalToRest(mergedApproval);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE, produces="application/json")
    public @ResponseBody void removeApproval(@PathVariable("id") Long id)
    {
    	approvalDao.removeApproval(id);
    } 

    @RequestMapping(value="/{id}/generate", method=RequestMethod.POST, produces="application/json")
    public @ResponseBody ApprovalRest generateProcess(@PathVariable("id") Long id)
    {
    	String processXml = approvalService.generateProcessDefinition(id);
    	logger.info("Process Xml\n\n" + processXml + "\n\n");
    	
    	// Update status
    	Approval approval = approvalDao.findById(id);
    	approval.setStatus(Approval.STATUS_GENERATED);
    	approval = approvalDao.saveApproval(approval);
    	
    	return convertApprovalToRest(approval);
    }
    
    @RequestMapping(value="/principals", method=RequestMethod.GET, produces="application/json")
    public @ResponseBody List<ApproverPrincipalRest> listAllPrincipals()
    {
    	List<ApproverPrincipalRest> principals = new ArrayList<ApproverPrincipalRest>();
    	
    	for(TaskUser user : userMgmt.findAllUsers()) 
    		principals.add(convertToPrincipal(user));
    
    	for(TaskGroup group : userMgmt.findAllGroups()) 
    		principals.add(convertToPrincipal(group));

        return principals;
    }
    
    
    /** 
     * Translation Methods 
     * TODO: Consider user Dozer for merge/translation instead
    **/
    
    private ApprovalRest convertApprovalToRest(Approval approval) {
    	ApprovalRest ret = new ApprovalRest();
    	ret.setId(approval.getId());
    	ret.setName(approval.getName());
    	ret.setDescription(approval.getDescription());
    	ret.setStatus(approval.getStatus());
    	ret.setCreated(approval.getCreated());
    	ret.setUpdated(approval.getUpdated());
    	ret.setProcessId(approval.getProcessId());
    	ret.setVersion(approval.getVersion());
    	for(ApprovalStep step : approval.getSteps())
    		ret.getSteps().add(convertStepToRest(step));
    	return ret;
    }
    
    private ApprovalStepRest convertStepToRest(ApprovalStep step) {
    	ApprovalStepRest ret = new ApprovalStepRest();
    	ret.setId(step.getId());
    	ret.setType(step.getType());
    	for(StepApprover approver : step.getApprovers())
    		ret.getApprovers().add(convertApproverToRest(approver));
    	return ret;
    }
    
    private StepApproverRest convertApproverToRest(StepApprover approver) {
    	StepApproverRest ret = new StepApproverRest();
    	ret.setId(approver.getId());
    	ret.setPrincipalId(approver.getPrincipalId());
    	ret.setLabel(approver.getLabel());
    	ret.setType(approver.getType());
    	return ret;
    }
    
    private ApproverPrincipalRest convertToPrincipal(TaskUser user) {
    	ApproverPrincipalRest approver = new ApproverPrincipalRest();
    	approver.setType(StepApprover.USER_TYPE);
    	approver.setPrincipalId(user.getId());
    	approver.setLabel("User: " + user.getDisplayName());
    	return approver;
    }
    
    private ApproverPrincipalRest convertToPrincipal(TaskGroup group) {
    	ApproverPrincipalRest approver = new ApproverPrincipalRest();
    	approver.setType(StepApprover.GROUP_TYPE);
    	approver.setPrincipalId(group.getId());
    	approver.setLabel("Group: " + group.getDisplayName());
    	return approver;
    }
    
    /**
     *  Translates the client side editable parameters from the REST to the JPA object
     */
    private Approval mergeApproval(Approval jpaApproval, ApprovalRest restApproval) {
    	jpaApproval.setName(restApproval.getName());
    	jpaApproval.setDescription(restApproval.getDescription());
    	
    	if(restApproval.getSteps() != null) {
    		// remove the jpa steps not in the rest array
    		List<ApprovalStep> stepsToRemove = new ArrayList<ApprovalStep>();
    		for(ApprovalStep step : jpaApproval.getSteps()) {
    			boolean hasMatch = false;
    			for(ApprovalStepRest restStep : restApproval.getSteps()) {
    				if(restStep.getId() != null && restStep.getId().equals(step.getId()))
    					hasMatch = true;
    			}
    			if(!hasMatch) {
    				stepsToRemove.add(step);
    			}
    		}
			jpaApproval.getSteps().removeAll(stepsToRemove);
    		
    		// create new steps and update existing ones
    		for(ApprovalStepRest restStep : restApproval.getSteps()) {
    			ApprovalStep jpaStep = null;
    			
    			// find existing matching step
    			if(restStep.getId() != null) {
    				for(ApprovalStep s : jpaApproval.getSteps()) {
    					if(s.getId().equals(restStep.getId()))
    						jpaStep = s;
    				}    					
    			}
    			
    			// create step if one doesn't exist in JPA
    			if(jpaStep == null) {
    				jpaStep = new ApprovalStep();
    				jpaApproval.addStep(jpaStep);
    			}
    			
    			// update the new or existing JPA step
				jpaStep.setType(restStep.getType());
    			
    			if(restStep.getApprovers() != null) {
    				// remove the jpa approvers not in the rest array
    				List<StepApprover> approversToRemove = new ArrayList<StepApprover>();
    	    		for(StepApprover jpaApprover : jpaStep.getApprovers()) {
    	    			boolean hasMatch = false;
    	    			for(StepApproverRest restApprover : restStep.getApprovers()) {
    	    				if(restApprover.getId() != null && restApprover.getId().equals(jpaApprover.getId()))
    	    					hasMatch = true;
    	    			}
    	    			if(!hasMatch) {
    	    				approversToRemove.add(jpaApprover);
    	    			}
    	    		}
    				jpaStep.getApprovers().removeAll(approversToRemove);
    	    		
    	    		// create new approvers and update existing ones
    	    		for(StepApproverRest restApprover : restStep.getApprovers()) {
    	    			StepApprover jpaApprover = null;
    	    			
    	    			// find existing matching step
    	    			if(restApprover.getId() != null) {
    	    				for(StepApprover sa : jpaStep.getApprovers()) {
    	    					if(sa.getId().equals(restApprover.getId()))
    	    						jpaApprover = sa;
    	    				}    					
    	    			}
    	    			
    	    			// create approver if one doesn't exist in JPA
    	    			if(jpaApprover == null) {
    	    				jpaApprover = new StepApprover();
    	    				jpaStep.addApprover(jpaApprover);
    	    			}

    	    			// update the new or existing JPA approver
	    				jpaApprover.setType(restApprover.getType());
	    				jpaApprover.setLabel(restApprover.getLabel());
	    				jpaApprover.setPrincipalId(restApprover.getPrincipalId());
    	    		}
    			}
    		}
    	}
    	return jpaApproval;
    } 
}
