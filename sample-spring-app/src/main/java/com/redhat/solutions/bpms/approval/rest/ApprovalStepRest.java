package com.redhat.solutions.bpms.approval.rest;

import java.io.Serializable;
import java.util.ArrayList;

public class ApprovalStepRest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String type;
	private ArrayList<StepApproverRest> approvers = new ArrayList<StepApproverRest>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ArrayList<StepApproverRest> getApprovers() {
		return approvers;
	}
	public void setApprovers(ArrayList<StepApproverRest> approvers) {
		this.approvers = approvers;
	}
	
	@Override
	public String toString() {
		return "ApprovalStepRest [id=" + id + ", type=" + type + "]";
	}
	
}
