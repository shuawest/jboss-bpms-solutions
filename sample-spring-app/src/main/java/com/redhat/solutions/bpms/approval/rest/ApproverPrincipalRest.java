package com.redhat.solutions.bpms.approval.rest;

import java.io.Serializable;

public class ApproverPrincipalRest implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String USER_TYPE = "user";
	public static final String GROUP_TYPE = "group";

	private String principalId;
	private String type;
	private String label;
	
	public String getPrincipalId() {
		return principalId;
	}
	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return "ApproverPrincipal [principalId=" + principalId + ", type=" + type + ", label=" + label + "]";
	}
	
}
