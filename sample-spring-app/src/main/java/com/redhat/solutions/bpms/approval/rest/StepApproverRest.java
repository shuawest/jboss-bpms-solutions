package com.redhat.solutions.bpms.approval.rest;

import java.io.Serializable;

public class StepApproverRest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String principalId;
	private String type;
	private String label;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPrincipalId() {
		return principalId;
	}
	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return "StepApproverRest [id=" + id + ", principalId=" + principalId + ", type=" + type + ", label=" + label + "]";
	}
}
