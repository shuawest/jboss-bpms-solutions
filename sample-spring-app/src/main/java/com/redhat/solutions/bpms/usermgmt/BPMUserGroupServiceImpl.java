package com.redhat.solutions.bpms.usermgmt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.kie.api.task.UserGroupCallback;
import org.kie.api.task.model.Group;
import org.kie.api.task.model.OrganizationalEntity;
import org.kie.internal.task.api.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;


public class BPMUserGroupServiceImpl implements UserGroupCallback, UserInfo {

	@Autowired
	private UserManagementService userManagement;

	@Override
	public String getDisplayName(OrganizationalEntity entity) {
		String displayName = "";
	
		TaskUser user = userManagement.findUser(entity.getId());
		if(user != null)
			displayName = user.getDisplayName();
		else {
			TaskGroup group = userManagement.findGroup(entity.getId());
			if(group != null) {
				displayName = group.getDisplayName();
			}
		}
		return displayName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterator<OrganizationalEntity> getMembersForGroup(Group group) {
		List users = userManagement.findUsersByGroup(group.getId());
		return (Iterator<OrganizationalEntity>)users.iterator();
	}

	@Override
	public boolean hasEmail(Group group) {
		boolean hasEmail = false;
		TaskGroup taskGroup = userManagement.findGroup(group.getId());
		if(group != null) {
			if(taskGroup.getEmail() != null & taskGroup.getEmail() != "")
				hasEmail = true;
		}
		return hasEmail;
	}

	@Override
	public String getEmailForEntity(OrganizationalEntity entity) {
		String email = "";
	
		TaskUser user = userManagement.findUser(entity.getId());
		if(user != null)
			email = user.getEmail();
		else {
			TaskGroup group = userManagement.findGroup(entity.getId());
			if(group != null) {
				email = group.getEmail();
			}
		}
		return email;
	}

	@Override
	public String getLanguageForEntity(OrganizationalEntity entity) {
		String language = "";
	
		TaskUser user = userManagement.findUser(entity.getId());
		if(user != null)
			language = user.getLanguage();
		else {
			TaskGroup group = userManagement.findGroup(entity.getId());
			if(group != null) {
				language = group.getLanguage();
			}
		}
		return language;
	}

	@Override
	public boolean existsUser(String userId) {
		TaskUser user = userManagement.findUser(userId);
		return (user != null);
	}

	@Override
	public boolean existsGroup(String groupId) {
		TaskGroup group = userManagement.findGroup(groupId);
		return (group != null);
	}

	@Override
	public List<String> getGroupsForUser(String userId, List<String> groupIds, List<String> allExistingGroupIds) {
		List<String> userGroupIds = new ArrayList<String>();
		
		TaskUser user = userManagement.findUser(userId);
		if(user != null) {
			userGroupIds = user.getGroupIds();
		}
		
		return userGroupIds;
	}
	
}
