package com.redhat.solutions.bpms.usermgmt;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.kie.api.task.model.Group;


@Entity
public class TaskGroup implements Group, Serializable  {

	private static final long serialVersionUID = 1L;

	public static final String ENGLISH_US = "en-US";

	@Id
	private String id;
	
	private String displayName;
	
	private String language;
	
	private String email;

    @OneToMany(mappedBy="group", fetch=FetchType.EAGER, cascade={CascadeType.ALL,CascadeType.REMOVE})
	private List<TaskGroupAssignment> userAssignments = new ArrayList<TaskGroupAssignment>();
	
	public TaskGroup() { }
	public TaskGroup(String id) {
		this.id = id;
		this.displayName = id;
		this.language = ENGLISH_US;
		this.email = null;
	}
	public TaskGroup(String id, String displayName, String language, String email) {
		this.id = id;
		this.displayName = displayName;
		this.language = language;
		this.email = email;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<TaskGroupAssignment> getUserAssignments() {
		return userAssignments;
	}
	public void setUserAssignments(List<TaskGroupAssignment> userAssignments) {
		this.userAssignments = userAssignments;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskGroup other = (TaskGroup) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "TaskGroup [id=" + id + "]";
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		throw new UnsupportedOperationException();
	}
}
