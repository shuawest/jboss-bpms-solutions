package com.redhat.solutions.bpms.usermgmt;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class TaskGroupAssignment implements Serializable  {

	private static final long serialVersionUID = 1L;

	public TaskGroupAssignment() {}
	
	public TaskGroupAssignment(TaskUser user, TaskGroup group) {
		this.user = user;
		this.group = group;
	}
	
	@Id
	@GeneratedValue
	private long id;
	
	@ManyToOne(optional=false, fetch=FetchType.EAGER)
	private TaskUser user;
	
	@ManyToOne(optional=false, fetch=FetchType.EAGER)
	private TaskGroup group;

	TaskUser getUser() {
		return user;
	}

	void setUser(TaskUser user) {
		this.user = user;
	}

	public TaskGroup getGroup() {
		return group;
	}

	public void setGroup(TaskGroup group) {
		this.group = group;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskGroupAssignment other = (TaskGroupAssignment) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TaskGroupAssignment [id=" + id + ", user=" + user + ", group=" + group + "]";
	}
}
