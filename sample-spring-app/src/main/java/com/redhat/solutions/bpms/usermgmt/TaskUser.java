package com.redhat.solutions.bpms.usermgmt;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.AccessType;
import org.kie.api.task.model.OrganizationalEntity;
import org.kie.api.task.model.User;
import org.springframework.web.bind.annotation.ResponseBody;

@Entity
public class TaskUser implements User, Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String ENGLISH_US = "en-US";

	@Id
	private String id;
	
	private String displayName;
	
	private String language;
	
	private String email;
	
    @OneToMany(mappedBy="user", fetch=FetchType.EAGER, cascade={CascadeType.ALL})
	private List<TaskGroupAssignment> groupAssignments = new ArrayList<TaskGroupAssignment>();

	public TaskUser() { }
	public TaskUser(String id) {
		this.id = id;
		this.displayName = id;
		this.language = ENGLISH_US;
		this.email = null;
	}
	public TaskUser(String id, String displayName, String language, String email) {
		this.id = id;
		this.displayName = displayName;
		this.language = language;
		this.email = email;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void addGroup(TaskGroup group) {
		groupAssignments.add(new TaskGroupAssignment(this, group));
	}
	public void removeGroup(TaskGroup group) {
		removeGroup(group.getId());
	}
	public void removeGroup(String groupId) {
		for(TaskGroupAssignment assignment : groupAssignments) {
			if(assignment.getGroup().getId() == groupId) {
				groupAssignments.remove(assignment);
				assignment.setUser(null);
				assignment.setGroup(null);
				break;
			}
		}
	}

	public List<TaskGroupAssignment> getGroupAssignments() {
		return groupAssignments;
	}
	public void setGroupAssignments(List<TaskGroupAssignment> groupAssignments) {
		this.groupAssignments = groupAssignments;
	}
	
	public List<TaskGroup> getGroups() {
		List<TaskGroup> groups = new ArrayList<TaskGroup>();
		for(TaskGroupAssignment assignment : groupAssignments) {
			groups.add(assignment.getGroup());
		}
		return groups;
	}
	public void setGroups(List<TaskGroup> groups) {
		groupAssignments = new ArrayList<TaskGroupAssignment>();
		for(TaskGroup group : groups) {
			addGroup(group);
		}
	}
	
	public List<String> getGroupIds() {
		List<String> groups = new ArrayList<String>();
		for(TaskGroupAssignment assignment : groupAssignments) {
			groups.add(assignment.getGroup().getId());
		}
		return groups;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskUser other = (TaskUser) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "TaskUser [id=" + id + "]";
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		throw new UnsupportedOperationException();
	}
}