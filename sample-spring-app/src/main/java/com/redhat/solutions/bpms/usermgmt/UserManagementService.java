package com.redhat.solutions.bpms.usermgmt;

import java.util.List;

public interface UserManagementService {

	TaskUser findUser(String userId);
	List<TaskUser> findAllUsers();
	List<TaskUser> findUsersByGroup(String groupId);
	void saveUser(TaskUser user);
	void removeUser(String userId);
	
	List<TaskGroupAssignment> findAssignmentsByGroup(String groupId);
	void removeAssignment(String userId, String groupId);
	
	TaskGroup findGroup(String groupId);
	List<TaskGroup> findAllGroups();
	void saveGroup(TaskGroup group);
	void removeGroup(String groupId);
		
}
