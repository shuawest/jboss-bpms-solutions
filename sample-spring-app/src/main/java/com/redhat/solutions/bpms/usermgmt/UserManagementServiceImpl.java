package com.redhat.solutions.bpms.usermgmt;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public class UserManagementServiceImpl implements UserManagementService {

	private static final Logger logger = LoggerFactory.getLogger(UserManagementServiceImpl.class);
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public TaskUser findUser(String userId) {
		logger.debug("finding user by id: {}", userId);
		TaskUser user = em.find(TaskUser.class, userId);

		logger.debug("Found user: {}", user);
		if(user != null) {
			Hibernate.initialize(user.getGroupAssignments());
			logger.info("Initialized assignments: {}", user.getGroupAssignments().size());
			for(TaskGroupAssignment assignment : user.getGroupAssignments()) {
				Hibernate.initialize(assignment.getGroup());
				logger.info("Initialized assignment {}", assignment);
			}
		}
		
		return user;
	}

	@Override
	public List<TaskUser> findAllUsers() {
		logger.debug("finding all users");
		TypedQuery<TaskUser> query = em.createQuery("SELECT u FROM TaskUser u", TaskUser.class);
       
		List<TaskUser> users = query.getResultList(); 
        for(TaskUser user : users) 
        	Hibernate.initialize(user.getGroupAssignments());
        
        return users;
	}

	@Override
	public List<TaskUser> findUsersByGroup(String groupId) {
		logger.info("finding users by group id: {}", groupId);
        TypedQuery<TaskUser> query = em.createQuery("SELECT u FROM TaskUser u, IN(u.groupAssignments) a WHERE a.group.id = :groupId", TaskUser.class);
        query.setParameter("groupId", groupId);
        
        List<TaskUser> users = query.getResultList(); 
        for(TaskUser user : users) 
        	Hibernate.initialize(user.getGroupAssignments());
        
        return users;
	}

	@Override
	public void saveUser(TaskUser user) {
		logger.info("Saving user: {}", user);
		em.merge(user);
	}

	@Override
	public void removeUser(String userId) {
		logger.info("Deleting user: {}", userId);
		
		Query delAssignment = em.createQuery("DELETE FROM TaskGroupAssignment a WHERE a.user.id = :userId");
		delAssignment.setParameter("userId", userId);
		delAssignment.executeUpdate();

		Query delGroup = em.createQuery("DELETE FROM TaskUser u WHERE u.id = :userId");
		delGroup.setParameter("userId", userId);
		delGroup.executeUpdate();
	}

	@Override
	public TaskGroup findGroup(String groupId) {
		logger.info("Finding group by id: {}", groupId);
		TaskGroup group = em.find(TaskGroup.class, groupId);
		if(group != null)
			Hibernate.initialize(group.getUserAssignments());
		return group;
	}

	@Override
	public List<TaskGroup> findAllGroups() {
		logger.info("Finding all groups");
		TypedQuery<TaskGroup> query = em.createQuery("SELECT g FROM TaskGroup g", TaskGroup.class);
        List<TaskGroup> groups = query.getResultList(); 
        for(TaskGroup group : groups) {
        	Hibernate.initialize(group.getUserAssignments());
        }
        return groups;
	}

	@Override
	public void saveGroup(TaskGroup group) {
		logger.info("Saving group: {}", group);
		em.merge(group);
	}

	@Override
	public void removeGroup(String groupId) {
		logger.info("Deleting group: {}", groupId);
		
		Query delAssignment = em.createQuery("DELETE FROM TaskGroupAssignment a WHERE a.group.id = :groupId");
		delAssignment.setParameter("groupId", groupId);
		delAssignment.executeUpdate();

		Query delGroup = em.createQuery("DELETE FROM TaskGroup g WHERE g.id = :groupId");
		delGroup.setParameter("groupId", groupId);
		delGroup.executeUpdate();
	}

	@Override
	public List<TaskGroupAssignment> findAssignmentsByGroup(String groupId) {
		TypedQuery<TaskGroupAssignment> query = em.createQuery("SELECT a FROM TaskGroupAssignment a WHERE a.group.id = :groupId", TaskGroupAssignment.class);
		query.setParameter("groupId", groupId);
		
		return query.getResultList(); 
	}
	
	@Override
	public void removeAssignment(String userId, String groupId) {
		logger.info("Deleting group assignment: userId={}, groupId={}", userId, groupId);

		Query delAssignment = em.createQuery("DELETE FROM TaskGroupAssignment a WHERE a.group.id = :groupId AND a.user.id = :userId");
		delAssignment.setParameter("userId", userId);
		delAssignment.setParameter("groupId", groupId);
		delAssignment.executeUpdate();
	}

}
