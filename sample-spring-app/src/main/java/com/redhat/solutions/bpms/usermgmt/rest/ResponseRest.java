package com.redhat.solutions.bpms.usermgmt.rest;

import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
public class ResponseRest<T> {
	
	public static final int OK = 200;
	public static final int NOT_FOUND = 404;
	public static final int INTERNAL_SERVER_ERROR = 500;
	public static final int CREATED = 201;
	public static final int NOT_MODIFIED = 304;
	public static final int BAD_REQUEST = 400;
	public static final int UNAUTHORIZED = 401;
	public static final int FORBIDDEN = 403;
	
	private int status = OK;
	private String message;
	private T response;

	public ResponseRest() {}
	public ResponseRest(int status, String message, T response) {
		this.status = status;
		this.message = message;
		this.response = response;
	}
	public ResponseRest(int status, String message) {
		this(status, message, null);
	}
	public ResponseRest(int status, T response) {
		this(status, "", response);
	}
	public ResponseRest(int status) {
		this(status, "", null);
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResponse() {
		return response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

	
}

