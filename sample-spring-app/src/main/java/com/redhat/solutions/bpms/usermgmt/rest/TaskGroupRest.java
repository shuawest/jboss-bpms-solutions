package com.redhat.solutions.bpms.usermgmt.rest;

import com.redhat.solutions.bpms.usermgmt.TaskGroup;

public class TaskGroupRest {
	private String id;
	private String displayName;
	private String language;
	private String email;
	private boolean isOriginal;

	public TaskGroupRest() {
	}

	public TaskGroupRest(TaskGroup group) {
		this.id = group.getId();
		this.displayName = group.getDisplayName();
		this.language = group.getLanguage();
		this.email = group.getEmail();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isOriginal() {
		return isOriginal;
	}

	public void setOriginal(boolean isOriginal) {
		this.isOriginal = isOriginal;
	}

}
