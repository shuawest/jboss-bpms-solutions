package com.redhat.solutions.bpms.usermgmt.rest;

import java.util.ArrayList;
import java.util.List;

import com.redhat.solutions.bpms.usermgmt.TaskUser;

public class TaskUserRest {

	private String id;
	private String displayName;
	private String language;
	private String email;
	private boolean isOriginal;
	private List<String> groups = new ArrayList<String>();

	public TaskUserRest() {
	}

	public TaskUserRest(TaskUser user) {
		this.id = user.getId();
		this.displayName = user.getDisplayName();
		this.language = user.getLanguage();
		this.email = user.getEmail();
		this.groups = user.getGroupIds();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isOriginal() {
		return isOriginal;
	}

	public void setOriginal(boolean isOriginal) {
		this.isOriginal = isOriginal;
	}

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

}
