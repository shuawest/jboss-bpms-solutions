package com.redhat.solutions.bpms.usermgmt.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.redhat.solutions.bpms.approval.ApprovalStep;
import com.redhat.solutions.bpms.approval.StepApprover;
import com.redhat.solutions.bpms.approval.rest.ApprovalStepRest;
import com.redhat.solutions.bpms.approval.rest.StepApproverRest;
import com.redhat.solutions.bpms.usermgmt.TaskGroup;
import com.redhat.solutions.bpms.usermgmt.TaskGroupAssignment;
import com.redhat.solutions.bpms.usermgmt.TaskUser;
import com.redhat.solutions.bpms.usermgmt.UserManagementService;

@Controller
@RequestMapping("/rest/user-management")
public class UserManagementRestController {

	private static final Logger logger = LoggerFactory.getLogger(UserManagementRestController.class);

	@Autowired
	private UserManagementService userManagement;

	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<TaskUserRest> listAllUsers() {
		List<TaskUserRest> restUsers = new ArrayList<TaskUserRest>();
		for (TaskUser user : userManagement.findAllUsers())
			restUsers.add(new TaskUserRest(user));

		return restUsers;
	}

	@RequestMapping(value = "/users/{userId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody
	TaskUserRest saveUser(@PathVariable("userId") String userId, @RequestBody TaskUserRest restUser) {
		logger.info("Saving user " + restUser.toString());

		TaskUser user = userManagement.findUser(restUser.getId());
		if (user == null)
			user = new TaskUser(restUser.getId());

		logger.info("User before update... \n " + printUser(user));

		// Update jpa user
		user.setDisplayName(restUser.getDisplayName());
		user.setEmail(restUser.getEmail());
		user.setLanguage(restUser.getLanguage());

		// create new assignments
		for (String groupId : restUser.getGroups()) {
			TaskGroupAssignment assignment = null;

			// find existing matching step
			for (TaskGroupAssignment a : user.getGroupAssignments()) {
				if (a.getGroup().getId().equals(groupId))
					assignment = a;
			}

			// create assignment if one doesn't exist in JPA
			if (assignment == null) {
				TaskGroup group = userManagement.findGroup(groupId);
				user.addGroup(group);
			}
		}

		userManagement.saveUser(user);

		// remove the jpa assignments not in the rest array
		List<TaskGroup> groupsToRemove = new ArrayList<TaskGroup>();
		for (TaskGroup group : user.getGroups()) {
			boolean hasMatch = false;
			for (String gid : restUser.getGroups()) {
				if (group.getId().equals(gid))
					hasMatch = true;
			}
			if (!hasMatch) {
				groupsToRemove.add(group);
			}
		}
		for (TaskGroup remGroup : groupsToRemove) {
			logger.info("Removing group assignment: " + remGroup);
			userManagement.removeAssignment(user.getId(), remGroup.getId());
		}

		TaskUser savedUser = userManagement.findUser(restUser.getId());

		logger.info("User after save... \n " + printUser(savedUser));

		return new TaskUserRest(savedUser);
	}

	private String printUser(TaskUser user) {
		String output = user.toString() + ",\n\tassignments: ";
		for (TaskGroupAssignment assignment : user.getGroupAssignments())
			output += "\n\t " + assignment.toString();
		return output;
	}

	@RequestMapping(value = "/users/{userId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	TaskUserRest findUser(@PathVariable("userId") String userId) {
		TaskUser user = userManagement.findUser(userId);

		TaskUserRest restUser = new TaskUserRest(user);
		return restUser;
	}

	@RequestMapping(value = "/users/{userId}", method = RequestMethod.DELETE, produces = "application/json")
	public @ResponseBody
	void removeUser(@PathVariable("userId") String userId) {
		userManagement.removeUser(userId);
	}

	@RequestMapping(value = "/users/{userId}/assignments", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<String> findUserAssignments(@PathVariable("userId") String userId) {
		TaskUser user = userManagement.findUser(userId);
		return user.getGroupIds();
	}

	@RequestMapping(value = "/users/{userId}/assignments", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	List<String> addUserAssignments(@PathVariable("userId") String userId, @RequestBody ArrayList<String> groupIds) {
		TaskUser user = userManagement.findUser(userId);

		for (String groupId : groupIds) {
			TaskGroup group = userManagement.findGroup(groupId);

			user.addGroup(group);
		}

		userManagement.saveUser(user);
		return findUserAssignments(userId);
	}

	@RequestMapping(value = "/users/{userId}/assignments/{groupId}", method = RequestMethod.DELETE, produces = "application/json")
	public @ResponseBody
	TaskUserRest removeUserAssignment(@PathVariable("userId") String userId, @PathVariable("groupId") String groupId) {

		userManagement.removeAssignment(userId, groupId);

		TaskUser retuser = userManagement.findUser(userId);
		return new TaskUserRest(retuser);
	}

	@RequestMapping(value = "/groups", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<TaskGroupRest> listAllGroups() {
		List<TaskGroupRest> restGroups = new ArrayList<TaskGroupRest>();
		for (TaskGroup group : userManagement.findAllGroups())
			restGroups.add(new TaskGroupRest(group));
		return restGroups;
	}

	@RequestMapping(value = "/groups/{groupId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody
	TaskGroupRest saveGroup(@PathVariable("groupId") String groupId, @RequestBody TaskGroupRest restGroup) {
		logger.info("Saving group " + restGroup.toString());
		TaskGroup group = new TaskGroup(restGroup.getId(), restGroup.getDisplayName(), restGroup.getLanguage(), restGroup.getEmail());
		userManagement.saveGroup(group);
		return restGroup;
	}

	@RequestMapping(value = "/groups/{groupId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	TaskGroupRest findGroup(@PathVariable("groupId") String groupId) {
		TaskGroup group = userManagement.findGroup(groupId);

		TaskGroupRest taskGroup = new TaskGroupRest(group);
		return taskGroup;
	}

	@RequestMapping(value = "/groups/{groupId}", method = RequestMethod.DELETE, produces = "application/json")
	public @ResponseBody
	void removeGroup(@PathVariable("groupId") String groupId) {
		TaskGroup group = userManagement.findGroup(groupId);

		userManagement.removeGroup(groupId);
	}

}
