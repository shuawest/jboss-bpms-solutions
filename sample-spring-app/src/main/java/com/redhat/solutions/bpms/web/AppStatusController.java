package com.redhat.solutions.bpms.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.redhat.solutions.bpms.AppStatusDao;
import com.redhat.solutions.bpms.approval.Approval;
import com.redhat.solutions.bpms.approval.ApprovalDao;

@Controller
@RequestMapping(value="/")
public class AppStatusController
{
    @Autowired
    private AppStatusDao appStatusDao;
    @Autowired
    private ApprovalDao approvalDao;

    @RequestMapping(method=RequestMethod.GET)
    public String displayStatusAndApprovals(Model model)
    {
        model.addAttribute("newApproval", new Approval());
    	loadModel(model);
        return "index";
    }

    @RequestMapping(method=RequestMethod.POST)
    public String registerNewMember(@ModelAttribute("newApproval") Approval newApproval, BindingResult result, Model model)
    {
        if (!result.hasErrors()) {
        	approvalDao.saveApproval(newApproval);
            return "redirect:/";
        }
        else {
        	loadModel(model);
            return "index";
        }
    }

    private void loadModel(Model model) {
        model.addAttribute("appStatus", appStatusDao.getAppStatus());
        model.addAttribute("approvals", approvalDao.findAll());    	
    }
}
