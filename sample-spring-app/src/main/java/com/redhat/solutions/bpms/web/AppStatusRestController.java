package com.redhat.solutions.bpms.web;

import org.kie.api.runtime.manager.RuntimeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.redhat.solutions.bpms.AppStatus;
import com.redhat.solutions.bpms.AppStatusDao;

@Controller
@RequestMapping("/rest/appStatus")
public class AppStatusRestController
{
	private static final Logger logger = LoggerFactory.getLogger(AppStatusRestController.class);
	
    @Autowired
    private AppStatusDao appStatusDao;

    @Autowired
    private RuntimeManager runtimeManager;
   
    @RequestMapping(method=RequestMethod.GET, produces="application/json")
    public @ResponseBody AppStatus getAppStatus()
    {
    	logger.debug("RM ID: " + runtimeManager.getIdentifier());
    	
        return appStatusDao.getAppStatus();
    }
}
