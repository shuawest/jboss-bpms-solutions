package com.redhat.solutions.bpms.web;

import java.util.List;

import org.kie.api.runtime.manager.RuntimeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.redhat.solutions.bpms.ProcessInfo;
import com.redhat.solutions.bpms.ProcessInstanceInfo;
import com.redhat.solutions.bpms.ProcessService;

@Controller
@RequestMapping("/rest/process")
public class ProcessRestController {

	private static final Logger logger = LoggerFactory.getLogger(ProcessRestController.class);
	
    @Autowired
    private ProcessService processService;
    
    @Autowired
    private RuntimeManager runtimeManager;

    @RequestMapping(value="/definition", method=RequestMethod.GET, produces="application/json")
    public @ResponseBody List<ProcessInfo> listAllProcesses()
    {
    	return processService.findAllProcesses();
    }

    @RequestMapping(value="/instance", method=RequestMethod.GET, produces="application/json")
    public @ResponseBody List<ProcessInstanceInfo>  listAllProcessesInstances()
    {
    	return processService.findAllProcessInstances();
    }
    
    @RequestMapping(value="/instance/start", method=RequestMethod.GET, produces="application/json")
    public @ResponseBody ProcessInstanceInfo startSampleProcessesInstance()
    {
    	return processService.startProcessInstance();
    }  

    @RequestMapping(value="/instance/startAndList", method=RequestMethod.GET, produces="application/json")
    public @ResponseBody List<ProcessInstanceInfo> startAndListProcessesInstances()
    {
    	ProcessInstanceInfo instance = processService.startProcessInstance();
    	return processService.findAllProcessInstances();
    }
}
