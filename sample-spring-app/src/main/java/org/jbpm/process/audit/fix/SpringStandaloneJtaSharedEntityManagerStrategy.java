package org.jbpm.process.audit.fix;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

import org.jbpm.process.audit.strategy.StandaloneJtaStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpringStandaloneJtaSharedEntityManagerStrategy extends StandaloneJtaStrategy {

	private static final Logger logger = LoggerFactory.getLogger(SpringStandaloneJtaSharedEntityManagerStrategy.class);

    private final EntityManager em;
    
    public SpringStandaloneJtaSharedEntityManagerStrategy(EntityManagerFactory emf) {
        super(null);
        this.em = emf.createEntityManager();
    }
    
    public SpringStandaloneJtaSharedEntityManagerStrategy(EntityManager em) {
        super(null);
        this.em = em;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
    @Override
    public void leaveTransaction(EntityManager em, Object transaction) {
        // do not close or clear the entity manager     
        commitTransaction(transaction);
    }
    
    @Override
    protected void commitTransaction(Object transaction) {
        UserTransaction ut = null;
        if(transaction != null) {
        	if(transaction instanceof UserTransaction) { 
        		ut = (UserTransaction) transaction;
        	} else {
        		throw new IllegalStateException("This persistence strategy only deals with UserTransaction instances!" );
        	}
        }
        
        try { 
            if( ut != null ) { 
                // There's a tx running, close it.
                ut.commit();
            }
        } catch(Exception e) { 
            logger.error("Unable to commit transaction: ", e);
        }
    }

}
