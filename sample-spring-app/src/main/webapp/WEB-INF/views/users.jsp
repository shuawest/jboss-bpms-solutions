<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>

<head>
<title>JBoss BPMS Spring Starter Application</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/app/assets/css/screen.css"/>' />
</head>

<body>
	<div id="container">
		<div class="dualbrand">
			<img src='<c:url value="/app/assets/gfx/dualbrand_logo.png"/>'
				alt="" />
		</div>
		<div id="content">
			<h1>JBoss BPMS Spring Starter Application</h1>
			<form:form commandName="newUser" id="reg">
				<h2>Create User</h2>
				<table>
					<tbody>
						<tr>
							<td><form:label path="id">Username:</form:label></td>
							<td><form:input path="id" /></td>
							<td><form:errors class="invalid" path="id" /></td>
						</tr>
						<tr>
							<td><form:label path="displayName">Display Name:</form:label></td>
							<td><form:input path="displayName" /></td>
							<td><form:errors class="invalid" path="displayName" /></td>
						</tr>
						<tr>
							<td><form:label path="email">Email:</form:label></td>
							<td><form:input path="email" /></td>
							<td><form:errors class="invalid" path="email" /></td>
						</tr>
						<tr>
							<td><form:label path="language">Language:</form:label></td>
							<td><form:input path="language" /></td>
							<td><form:errors class="invalid" path="language" /></td>
						</tr>
					</tbody>
				</table><h2>Create Group</h2>
				<table>
					<tbody>
						<tr>
							<td><form:label path="id">Group Name:</form:label></td>
							<td><form:input path="id" /></td>
							<td><form:errors class="invalid" path="id" /></td>
						</tr>
						<tr>
							<td><form:label path="displayName">Display Name:</form:label></td>
							<td><form:input path="displayName" /></td>
							<td><form:errors class="invalid" path="displayName" /></td>
						</tr>
						<tr>
							<td><form:label path="email">Email:</form:label></td>
							<td><form:input path="email" /></td>
							<td><form:errors class="invalid" path="email" /></td>
						</tr>
						<tr>
							<td><form:label path="language">Language:</form:label></td>
							<td><form:input path="language" /></td>
							<td><form:errors class="invalid" path="language" /></td>
						</tr>
					</tbody>
				</table>
				<table>
					<tr>
						<td><input type="submit" value="Create" class="register" /></td>
					</tr>
				</table>
			</form:form>
			<h2>Approvals</h2>
			<c:choose>
				<c:when test="${approvals.size()==0}">
					<em>There are no approvals.</em>
				</c:when>
				<c:otherwise>
					<table class="simpletablestyle">
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Number of Steps</th>
								<th>REST URL</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${approvals}" var="approval">
								<tr>
									<td>${approval.id}</td>
									<td>${approval.name}</td>
									<td>${approval.stepCount}</td>
									<td><a
										href='<c:url value="/rest/approval/${approval.id}"/>'>/rest/approval/${approval.id}</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<table class="simpletablestyle">
						<tr>
							<td>REST URL for all approvals: <a
								href='<c:url value="/rest/approval"/>'>/rest/approval</a>
							</td>
						</tr>
					</table>
				</c:otherwise>
			</c:choose>
		</div>
		<div id="aside">
			<p>Learn more about JBoss BPMS 6</p>
			<ul>
				<li><a
					href="https://access.redhat.com/documentation/en-US/Red_Hat_JBoss_BPM_Suite/6.0/html-single/Development_Guide/index.html">Development
						Guide</a></li>
				<li><a
					href="https://access.redhat.com/documentation/en-US/Red_Hat_JBoss_BPM_Suite/6.0/html-single/Administration_And_Configuration_Guide/index.html">Administration
						Guide</a></li>
			</ul>
		</div>
		<div id="footer">
			<p>Created by Josh West jowest@redhat.com</p>
		</div>
	</div>
</body>
</html>
