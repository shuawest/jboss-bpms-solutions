var app = angular.module("sample-spring-app", 
		["ngRoute", 
		 "app.controllers", "app.services", "app.filters", "app.directives"]
);

app.config(function($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix('!');
	$routeProvider. 
		when("/approvals", { templateUrl: "assets/partials/approvals.html", controller: "approvalsCtrl" }).
	    when("/approvals/:approvalId", { templateUrl: "assets/partials/approval-detail.html", controller: "approvalDetailCtrl" }).
	    when("/approvals/:approvalId/view", { templateUrl: "assets/partials/approval-viewer.html", controller: "approvalViewerCtrl" }).
	    when("/user-mgmt", { templateUrl: "assets/partials/user-mgmt.html", controller: "userMgmtCtrl" }).
	    when("/group-mgmt", { templateUrl: "assets/partials/group-mgmt.html", controller: "groupMgmtCtrl" }).
	    when("/about", { templateUrl: "assets/partials/about.html", controller: "aboutCtrl" }).
	    otherwise( { redirectTo: "/approvals" });
});   

