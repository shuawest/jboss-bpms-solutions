var ctlrs = angular.module("app.controllers", []);

ctlrs.controller("navCtrl", function($scope, $location) {
	$scope.menuClass = function(page) {
		var current = $location.path().substring(1);
		return page === current ? "active" : "";
	};
});

ctlrs.controller("approvalsCtrl", function($scope, $http, Approval) {

	$scope.model = {
		initialized : false,
		issues : [],
		approvals : []
	};

	$scope.removeApproval = function(index) {
		$scope.model.approvals[index].$remove();
		$scope.model.approvals.splice(index, 1);
	};

	Approval.query(function(response) {
		$scope.model.initialized = true;
		$scope.model.approvals = response;
	}, function(errmsg) {
		$scope.model.initialized = true;
		$scope.model.issues = [ {
			field : "approval",
			msg : errmsg.status + " " + errmsg.statusText,
			type : "danger"
		} ];
	});

});

ctlrs.controller("approvalViewerCtrl", function($scope, $routeParams, Approval) {
	$scope.model = {
		initialized : false,
		issues : [],
		approvalId : $routeParams.approvalId,
		approval : null
	};
	
	Approval.get({
		id : $routeParams.approvalId
	}, function(response) {
		if (response != null && response.id > 0) {
			$scope.model.approval = response;
		}
		$scope.model.initialized = true;
	}, function(errmsg) {
		$scope.model.initialized = true;
		$scope.model.issues = [ {
			field : "approval",
			msg : errmsg.status + " " + errmsg.statusText,
			type : "danger"
		} ];
	});
	
});

ctlrs.controller("approvalDetailCtrl", function($scope, $routeParams, Approval,
		ApproverPrincipal) {

	$scope.createEmptyApproval = function() {
		var a = new Approval();
		a.name = null;
		a.description = null;
		a.created = new Date();
		a.updated = new Date();
		a.status = "new";
		a.processId = null;
		a.version = 1.0;
		a.steps = [];
		return a;
	};

	$scope.model = {
		initialized : false,
		isDirty : false,
		isValid : true,
		issues : [],
		approvalId : $routeParams.approvalId,
		principalList : ApproverPrincipal.query(),
		approval : $scope.createEmptyApproval()
	};

	$scope.markDirty = function() {
		$scope.model.isDirty = true;
		$scope.model.isValid = true;
		$scope.model.issues = [];
	};

	$scope.validate = function() {
		var isValid = true;
		var issues = [];
		var approval = $scope.model.approval;

		if (approval.name == null || approval.name == "") {
			issues.push({
				field : "approval.name",
				msg : "Approval name is not set.",
				type : "danger"
			});
			isValid = false;
		}
		if (approval.description == null || approval.description == "") {
			issues.push({
				field : "approval.description",
				msg : "Approval description is not set.",
				type : "danger"
			});
			isValid = false;
		}
		if (approval.steps == null || approval.steps.length == 0) {
			issues.push({
				field : "approval.steps",
				msg : "There no steps defined for the approval.",
				type : "warning"
			});
		} else {
			for ( var i in approval.steps) {
				var step = approval.steps[i];
				if (step.approvers.length == 0) {
					var index = approval.steps.indexOf(step) + 1;
					issues.push({
						field : "approval.steps[" + index + "].approvers",
						msg : "There no appovers added to Step " + index + ".",
						type : 'warning'
					});
				}
			}
		}

		$scope.model.isValid = isValid;
		$scope.model.issues = issues;
	};

	var saveCallback = function(response) {
		$scope.model.isDirty = false;
		$scope.model.initialized = true;
	};
	var errorCallback = function(errmsg) {
		$scope.model.isDirty = true;
		$scope.model.initialized = true;
		$scope.model.issues = [ {
			field : "approval",
			msg : errmsg.status + " " + errmsg.statusText,
			type : "danger"
		} ];
	};
	
	$scope.saveApproval = function() {
		$scope.validate();
		if ($scope.model.isValid) {
			$scope.model.initialized = false;

			if ($scope.model.approval.id) {
				$scope.model.approval.$update(saveCallback, errorCallback);
			} else {
				$scope.model.approval.$save(saveCallback, errorCallback);
			}
		}
	};

	$scope.generateProcess = function() {
		$scope.model.approval.$generate(saveCallback, errorCallback);
	};

	$scope.changedName = function() {
		$scope.markDirty();
	};

	$scope.changedDescription = function() {
		$scope.markDirty();
	};

	$scope.addStep = function() {
		if ($scope.model.approval.steps == null)
			$scope.model.approval.steps = [];

		$scope.model.approval.steps.push({
			type : 'all',
			approvers : []
		});

		$scope.markDirty();
	};

	$scope.removeStep = function(step) {
		var stepIndex = $scope.model.approval.steps.indexOf(step);
		$scope.model.approval.steps.splice(stepIndex, 1);
		$scope.markDirty();
	};

	$scope.addApprover = function(step, principal) {
		if (step.approvers == null)
			step.approvers = [];

		step.approvers.push(principal);
		$scope.markDirty();
	};

	$scope.removeApprover = function(step, approver) {
		var aprIndex = step.approvers.indexOf(approver);
		step.approvers.splice(aprIndex, 1);
		$scope.markDirty();
	};

	$scope.setStepType = function(step, type) {
		step.type = type;
		$scope.markDirty();
	};

	if ($routeParams.approvalId == 'new') {
		$scope.model.initialized = true;
	} else {
		Approval.get({
			id : $routeParams.approvalId
		}, function(response) {
			if (response != null && response.id > 0) {
				$scope.model.approval = response;
			}
			$scope.model.initialized = true;
		}, function(errmsg) {
			$scope.model.isDirty = true;
			$scope.model.initialized = true;
			$scope.model.issues = [ {
				field : "approval",
				msg : errmsg.status + " " + errmsg.statusText,
				type : "danger"
			} ];
		});
	}

});

ctlrs.controller("userMgmtCtrl", function($scope, $http, User, Group) {

	$scope.model = {
		initialized : false,
		issues : [],
		users : [],
		groups : []
	};

	var uniqueGroupFilter = function(groupIds) {
		if(groupIds != null) {
			var uniqeIds = groupIds.filter(function(elem, pos) {
			    return groupIds.indexOf(elem) == pos;
			});
		}
		return uniqeIds;
	};
	
	var loadUsersCallback = function(response) {
		if (response != null) {
			$scope.model.users = response;
			for(var user in $scope.model.users) {
				user.groups = uniqueGroupFilter(user.groups);
			}
		}
		$scope.model.initialized = true;
	};

	var errorUsersCallback = function(errmsg) {
		$scope.model.initialized = true;
		$scope.model.issues = [ {
			field : "users",
			msg : errmsg.status + " " + errmsg.statusText,
			type : "danger"
		} ];
	};
	
	var loadGroupsCallback = function(response) {
		if (response != null) {
			$scope.model.groups = response;
		}
		$scope.model.initialized = true;
	};

	var errorGroupsCallback = function(errmsg) {
		$scope.model.initialized = true;
		$scope.model.issues = [ {
			field : "groups",
			msg : errmsg.status + " " + errmsg.statusText,
			type : "danger"
		} ];
	};

	$scope.addUser = function() {
		var user = new User();
		user.original = true;
		user.language = "en-US";
		$scope.model.users.unshift(user);
	};

	$scope.saveUser = function(user) {
		if (user.id != null) {
			var saveCallback = function(response) {
				user.original = false;
			};
			user.$save(saveCallback, errorUsersCallback);
		}
	};

	$scope.removeUser = function(user) {
		var userIndex = $scope.model.users.indexOf(user);
		$scope.model.users.splice(userIndex, 1);
		user.$remove();
	};

	$scope.addAssignment = function(user, assignment) {
		if(user.groups == null)
			user.groups = [];
		
		if(user.groups.indexOf(assignment.id) == -1)		
			user.groups.push(assignment.id);
	};

	$scope.removeAssignment = function(user, assignmentId) {
		var aindex = user.groups.indexOf(assignmentId);
		user.groups.splice(aindex, 1);
	};

	User.query(loadUsersCallback, errorUsersCallback);
	
	Group.query(loadGroupsCallback, errorGroupsCallback);
});

ctlrs.controller("groupMgmtCtrl", function($scope, $http, Group) {

	$scope.model = {
		initialized : false,
		issues : [],
		groups : []
	};

	var errorCallback = function(errmsg) {
		$scope.model.initialized = true;
		$scope.model.issues = [ {
			field : "groups",
			msg : errmsg.status + " " + errmsg.statusText,
			type : "danger"
		} ];
	};

	$scope.addGroup = function() {
		var group = new Group();
		group.original = true;
		group.language = "en-US";
		$scope.model.groups.unshift(group);
	};

	$scope.saveGroup = function(group) {
		if (group.id != null) {
			var saveCallback = function(response) {
				group.original = false;
			};
			group.$save(saveCallback, errorCallback);
		}
	};

	$scope.removeGroup = function(group) {
		var groupIndex = $scope.model.groups.indexOf(group);
		$scope.model.groups.splice(groupIndex, 1);
		group.$remove();
	};

	var loadGroupsCallback = function(response) {
		if (response != null) {
			$scope.model.groups = response;
		}
		$scope.model.initialized = true;
	};

	Group.query(loadGroupsCallback, errorCallback);
});


ctlrs.controller("aboutCtrl", function($scope, $http) {

});
