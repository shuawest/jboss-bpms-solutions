var filterModule = angular.module("app.filters", []);

filterModule.filter("markdown", function($sce) {
	var converter = new Showdown.converter();
	return function(value) {
		var html = converter.makeHtml(value || '');
		html = html.replace("\n", "<br />");
		return $sce.trustAsHtml(html);
	};
});

filterModule.filter('iif', function() {
	return function(input, trueValue, falseValue) {
		return input ? trueValue : falseValue;
	};
});