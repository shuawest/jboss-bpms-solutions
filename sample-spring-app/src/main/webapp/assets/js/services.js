var serviceModule = angular.module("app.services", [ "ngResource" ]);

serviceModule.factory("Approval", function($resource) {
	return $resource("./rest/approvals/:id/:command", { id: '@id', command: '@command' }, 
	{ 
		find: { method: "GET" },
		update: { method: "PUT"	},
		remove: { method: "DELETE" },
		generate: { method: "POST", params: { command: 'generate' } }
	});
});

serviceModule.factory("User", function($resource) {
	return $resource("./rest/user-management/users/:id", { id: '@id'}, 
		{ 
			find: { method: "GET" },
			remove: { method: "DELETE" }
		});
});

serviceModule.factory("Group", function($resource) {
	return $resource("./rest/user-management/groups/:id", { id: '@id'}, 
		{ 
			find: { method: "GET" },
			remove: { method: "DELETE" }
		});
});

serviceModule.factory("ApproverPrincipal", function($resource) {
	return $resource("./rest/approvals/principals", { id: '@id'});
});
