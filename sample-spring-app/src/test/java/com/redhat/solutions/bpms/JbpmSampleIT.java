package com.redhat.solutions.bpms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.List;

import org.jbpm.process.audit.AuditLogService;
import org.jbpm.process.audit.ProcessInstanceLog;
import org.jbpm.services.task.impl.model.GroupImpl;
import org.jbpm.services.task.impl.model.UserImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.definition.process.Process;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.Task;
import org.kie.internal.task.api.InternalTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.redhat.solutions.bpms.usermgmt.TaskGroup;
import com.redhat.solutions.bpms.usermgmt.TaskUser;
import com.redhat.solutions.bpms.usermgmt.UserManagementService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext1.xml")
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
@Transactional
public class JbpmSampleIT {
	
	private final Logger logger = LoggerFactory.getLogger(JbpmSampleIT.class);

	@Autowired
	private RuntimeManager runtimeManager;

	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private UserManagementService userManagement;
	
	
	@Before
	public void setup() {
		createRequiredUsers();
		createTestUsers();
	}
	
	private void createRequiredUsers() {
		TaskGroup admins = userManagement.findGroup("Administrators");
		if(admins == null) {
			admins = new TaskGroup("Administrators", "Administrators", "en-US", null);
			userManagement.saveGroup(admins);
		}
		
		TaskUser admin = userManagement.findUser("admin");
		if(admin == null) {
			admin = new TaskUser("admin", "Business Administrator", "en-US", "admin@redhat.com");
			admin.addGroup(admins);
			userManagement.saveUser(admin);
		}
	}

	private void createTestUsers() {
		TaskGroup taskdoer = userManagement.findGroup("taskdoer");
		if(taskdoer == null) {
			taskdoer = new TaskGroup("taskdoer", "taskdoer", "en-US", null);
			userManagement.saveGroup(taskdoer);
		}
		
		TaskUser jowest = userManagement.findUser("jowest");
		if(jowest == null) {
			jowest = new TaskUser("jowest", "Josh West", "en-US", "jowest@redhat.com");
			jowest.addGroup(taskdoer);
			userManagement.saveUser(jowest);
		}
	}
	
	@Test
	public void sessionTest() {	
		RuntimeEngine engine = runtimeManager.getRuntimeEngine(null);
        KieSession ksession = engine.getKieSession();
		assertNotNull(ksession);
	}
	
	@Test
	public void processTest() {	
		RuntimeEngine engine = runtimeManager.getRuntimeEngine(null);
        KieSession ksession = engine.getKieSession();
		
        Collection<Process> processes = ksession.getKieBase().getProcesses();
		assertEquals(1, processes.size());
		
		Process process = (Process)processes.toArray()[0];
		assertNotNull(process);
	}
	
	@Test
	public void startProcessInstanceTest() {	
		RuntimeEngine engine = runtimeManager.getRuntimeEngine(null);
        KieSession ksession = engine.getKieSession();
        
        ProcessInstance processInstance = ksession.startProcess("com.redhat.solutions.bpms.sample2");
        
        assertNotNull(processInstance);
        assertNotEquals(0, processInstance.getState());
	}

	@Test
	public void getProcessInstancesTest() {	
		RuntimeEngine engine = runtimeManager.getRuntimeEngine(null);
        KieSession ksession = engine.getKieSession();
        ProcessInstance processInstance = ksession.startProcess("com.redhat.solutions.bpms.sample2");
        assertNotNull(processInstance);
        
        Collection<ProcessInstance> instances = ksession.getProcessInstances();
        
        int index = 0;
        for(ProcessInstance instance : instances) {
        	logger.info(String.format("Instance %d: id=%d, state=%s", 
        			++index, 
        			instance.getId(), 
        			instance.getProcessName()));
        }
        
        assertEquals(1, instances.size());
	} 
	
	@Test
	public void getProcessInstancesLogTest() {	
        List<ProcessInstanceLog> instanceLogsBefore = auditLogService.findProcessInstances();
        int logSizeBefore = instanceLogsBefore.size();
        
		RuntimeEngine engine = runtimeManager.getRuntimeEngine(null);
        KieSession ksession = engine.getKieSession();
        ProcessInstance processInstance = ksession.startProcess("com.redhat.solutions.bpms.sample2");
        assertNotNull(processInstance);
        
        Collection<ProcessInstance> instances = ksession.getProcessInstances();
        assertEquals("Intance in session size",1, instances.size());
             
        List<ProcessInstanceLog> instanceLogsAfter = auditLogService.findProcessInstances();
        assertEquals("Intance log size", ++logSizeBefore, instanceLogsAfter.size());
	} 
	
	@Test
	public void completeTaskTest() {	
		RuntimeEngine engine = runtimeManager.getRuntimeEngine(null);
        KieSession ksession = engine.getKieSession();
        ProcessInstance processInstance = ksession.startProcess("com.redhat.solutions.bpms.sample2");
        
        List<Long> taskIds = taskService.getTasksByProcessInstanceId(processInstance.getId());
        assertEquals(1, taskIds.size());
        
        Long taskId = taskIds.get(0);
        
        Task task = taskService.getTaskById(taskId);
        assertNotNull(task);
        
        taskService.claim(taskId, "jowest");
        taskService.start(taskId, "jowest");
        taskService.complete(taskId, "jowest", null);
        
        ProcessInstanceLog instanceLog = auditLogService.findProcessInstance(processInstance.getId());
        assertNotNull(instanceLog);
        assertEquals(ProcessInstance.STATE_COMPLETED, instanceLog.getStatus().intValue());
	}
}
