package com.redhat.solutions.bpms;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.redhat.solutions.bpms.ProcessInstanceInfo;
import com.redhat.solutions.bpms.ProcessService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext1.xml")
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
@Transactional
public class ProcessServiceIT {

	@Autowired
	private ProcessService processService;
	
	@Test
	public void createProcessInstance() {
		ProcessInstanceInfo instance = processService.startProcessInstance();
		assertNotEquals(0, instance.getState());
	}
	
	@Test
	public void createAndListProcessInstance() {
		ProcessInstanceInfo instance = processService.startProcessInstance();
		List<ProcessInstanceInfo> instances = processService.findAllProcessInstances();
		
		ProcessInstanceInfo match = findById(instances, instance.getId());
		assertNotNull(match);
		assertNotEquals(0, instance.getState());
	}
	
	private ProcessInstanceInfo findById(List<ProcessInstanceInfo> instances, Long id) {
		for (ProcessInstanceInfo instance : instances) {
			if(instance.getId().equals(id))
				return instance;
		}
		return null;
	}
}
