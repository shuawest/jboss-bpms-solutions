package com.redhat.solutions.bpms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.redhat.solutions.bpms.approval.Approval;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext1.xml")
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
@Transactional
public class ProjectIT {
	
	private final Logger logger = LoggerFactory.getLogger(ProjectIT.class);
	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private AppStatusDao approvalDao;
	
	@Test
	public void jndiTest() throws NamingException {		
		InitialContext context = new InitialContext();
		assertNotNull(context);
		
		Object jbpmDS = context.lookup("jdbc/jbpmAppDS");
		assertNotNull(jbpmDS);
	}
	
	@Test
	public void hasApprovalEntity() {
		AppStatus appStatus = approvalDao.getAppStatus();
		List<String> entities = appStatus.getDatabaseEntities();
		assertEquals(true, entities.contains(Approval.class.getName()));
	}
	
	@Test
	public void loggerTest() {
		// check logs for this text to verify logging dependencies
		logger.info("loggerTest output at info level");
		assertNotNull(logger); 
	}	
}
