package com.redhat.solutions.bpms.approval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext1.xml")
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
@Transactional
public class ApprovalDaoIT {

	private static final String REF_NAME = "reference approval";
	private static final String REF_DESCRIPTION = "reference approval description";
	private static final Date REF_DATE = new Date();
	private static final String REF_PROCESS_ID = "com.redhat.solutions.bpms.Approval1";
	private static final String REF_VERSION = "1.0";
	
	private Approval refApproval;
	private ApprovalStep refStep1;
	private ApprovalStep refStep2;
	private StepApprover refApprover1;
	private StepApprover refApprover2;
	private StepApprover refApprover3;
	
	@Autowired
	private ApprovalDao approvalDao;

	@Before
	public void setup() {
		createReferenceApproval();
		approvalDao.saveApproval(refApproval);
	}
	
	@Test
	public void findAllTest() {
		List<Approval> approvals = approvalDao.findAll();
		assertReferenceApproval(approvals);
	}
	
	@Test
	public void saveTest() {
		List<Approval> approvals = approvalDao.findAll();
		Approval approval = findApprovalByName(approvals, REF_NAME);
		
		assertReferenceApproval(approval);
		assertReferenceSteps(approval);
	}
	
	@Test
	public void removeStepTest() {
		List<Approval> premodApprovals = approvalDao.findAll();
		Approval premodApproval = findApprovalByName(premodApprovals, REF_NAME);

		ApprovalStep step = premodApproval.getSteps().get(1);
		premodApproval.removeStep(step);
		
		approvalDao.saveApproval(premodApproval);
		
		List<Approval> modApprovals = approvalDao.findAll();
		Approval modApproval = findApprovalByName(modApprovals, REF_NAME);
		
		assertReferenceApproval(modApproval);
		assertEquals(1, modApproval.getSteps().size());
	}
	

	@Test
	public void removeApproverTest() {
		List<Approval> premodApprovals = approvalDao.findAll();
		Approval premodApproval = findApprovalByName(premodApprovals, REF_NAME);

		ApprovalStep premodStep = premodApproval.getSteps().get(0);
		StepApprover approver = premodStep.getApprovers().get(0);
		premodStep.removeApprover(approver);
		
		approvalDao.saveApproval(premodApproval);
		
		List<Approval> modApprovals = approvalDao.findAll();
		Approval modApproval = findApprovalByName(modApprovals, REF_NAME);
		
		assertReferenceApproval(modApproval);
		assertEquals(2, modApproval.getSteps().size());
		assertEquals(1, modApproval.getSteps().get(0).getApprovers().size());
	}
	
	@Test
	public void findByIdTest() {
		List<Approval> approvals = approvalDao.findAll();
		Approval lookupApproval = findApprovalByName(approvals, REF_NAME);
		
		Approval approval = approvalDao.findById(lookupApproval.getId());
		assertReferenceApproval(approval);
		assertReferenceSteps(approval);
	}
	
	
	private void createReferenceApproval() {
		refApproval = new Approval();
		refApproval.setName(REF_NAME);
		refApproval.setDescription(REF_DESCRIPTION);
		refApproval.setCreated(REF_DATE);
		refApproval.setUpdated(REF_DATE);
		refApproval.setProcessId(REF_PROCESS_ID);
		refApproval.setStatus(Approval.STATUS_NEW);
		refApproval.setVersion(REF_VERSION);
		
		refStep1 = new ApprovalStep();
		refStep1.setType(ApprovalStep.ALL_APPROVE);
		
		refStep2 = new ApprovalStep();
		refStep2.setType(ApprovalStep.ANY_APPROVE);
		
		refApprover1 = new StepApprover();
		refApprover1.setLabel("User: Josh West");
		refApprover1.setPrincipalId("jowest");
		refApprover1.setType(StepApprover.USER_TYPE);

		refApprover2 = new StepApprover();
		refApprover2.setLabel("User: Derrick Kittler");
		refApprover2.setPrincipalId("dkittler");
		refApprover2.setType(StepApprover.USER_TYPE);
		
		refApprover3 = new StepApprover();
		refApprover3.setLabel("Group: NA East Middleware SAs");
		refApprover3.setPrincipalId("sa-mw-east");
		refApprover3.setType(StepApprover.GROUP_TYPE);
		
		refApproval.addStep(refStep1);
		refStep1.addApprover(refApprover1);
		refStep1.addApprover(refApprover2);
		
		refApproval.addStep(refStep2);	
		refStep2.addApprover(refApprover3);
	}
	
	private void assertReferenceApproval(Approval approval) {
		assertNotNull(approval);
		assertNotNull(approval.getId());
		assertEquals(REF_NAME, approval.getName());
		assertEquals(REF_DESCRIPTION, approval.getDescription());
		assertEquals(REF_DATE, approval.getCreated());
		assertEquals(REF_DATE, approval.getUpdated());
		assertEquals(REF_PROCESS_ID, approval.getProcessId());
		assertEquals(Approval.STATUS_NEW, approval.getStatus());
		assertEquals(REF_VERSION, approval.getVersion());	
		assertNotNull(approval.getSteps());
		
	}
	
	private void assertReferenceSteps(Approval approval) {
		assertEquals(2, approval.getSteps().size());
		
		ApprovalStep step1 = approval.getSteps().get(0);
		assertNotNull(step1.getId());
		assertEquals(approval, step1.getApproval());
		assertEquals(ApprovalStep.ALL_APPROVE, step1.getType());
		assertEquals(2, step1.getApprovers().size());

		ApprovalStep step2 = approval.getSteps().get(1);
		assertNotNull(step2.getId());
		assertEquals(approval, step2.getApproval());
		assertEquals(ApprovalStep.ANY_APPROVE, step2.getType());
		assertEquals(1, step2.getApprovers().size());
	}

	private void assertReferenceApproval(List<Approval> approvals) {
		assertNotNull(approvals);
		Approval approval = findApprovalByName(approvals, REF_NAME);		
		assertReferenceApproval(approval);
		assertReferenceSteps(approval);
	}
	
	private Approval findApprovalByName(List<Approval> approvals, String name) {
		for(Approval a : approvals) {
			if(name.equals(a.getName())) {
				return a;
			}
		}
		return null;
	}
}
