package com.redhat.solutions.bpms.approval;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jbpm.bpmn2.xml.XmlBPMNProcessDumper;
import org.jbpm.ruleflow.core.RuleFlowProcess;
import org.jbpm.ruleflow.core.RuleFlowProcessFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.io.Resource;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.redhat.solutions.bpms.usermgmt.TaskGroup;
import com.redhat.solutions.bpms.usermgmt.TaskUser;
import com.redhat.solutions.bpms.usermgmt.UserManagementService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext1.xml")
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
@Transactional
public class ApprovalServiceIT {

	private final Logger logger = LoggerFactory.getLogger(ApprovalServiceIT.class);

	@Autowired
	private ApprovalService approvalService;
	
	@Autowired
	private ApprovalDao approvalDao;
	
	@Autowired
	private UserManagementService userMgmt;
	
	private TaskUser user1;
	private TaskUser user2;
	private TaskGroup group1;
	private TaskGroup group2;	
	
	@Test
	public void generateProcess() {
		Approval approval = createTestApproval1();
		
		String processXml = approvalService.generateProcessDefinition(approval.getId());
	
		logger.info("Output:\n" + processXml);
	}

	@Test
	public void createProcess() throws IOException {
    	RuleFlowProcessFactory factory = RuleFlowProcessFactory.createProcess("org.jbpm.process");
        
    	factory
                // header
                .name("My process").packageName("org.jbpm")
                // nodes
                .startNode(1).name("Start").done()
                .actionNode(2).name("Action").action("java", "System.out.println(\"Action\");").done()
                .endNode(3).name("End").done()
                // connections
                .connection(1, 2)
                .connection(2, 3);
        RuleFlowProcess process = factory.validate().getProcess();
        
        process.getNode(1).getMetaData().put("x", 30);
        process.getNode(1).getMetaData().put("y", 126);
        
        process.getNode(2).getMetaData().put("x", 160);
        process.getNode(2).getMetaData().put("y", 110);
        process.getNode(2).getMetaData().put("width", 130);
        process.getNode(2).getMetaData().put("height", 80);
        
        process.getNode(3).getMetaData().put("x", 360);
        process.getNode(3).getMetaData().put("y", 126);
        
        Resource res = ResourceFactory.newByteArrayResource(XmlBPMNProcessDumper.INSTANCE.dump(process, XmlBPMNProcessDumper.NO_META_DATA).getBytes());
        res.setSourcePath("/tmp/processFactory.bpmn2"); // source path or target path must be set to be added into kbase
        
        String bpmn2xml = IOUtils.toString(res.getReader());

        logger.info("Output:\n" + bpmn2xml);
        
//        KieBase kbase = createKnowledgeBaseFromResources(res);
//        StatefulKnowledgeSession ksession = createKnowledgeSession(kbase);
//        ksession.startProcess("org.jbpm.process");
//        ksession.dispose();

    }

	
	/** Test Data Construction **/
	
	@Before
	public void before() {
		List<TaskUser> users = userMgmt.findAllUsers();
		user1 = users.get(0);
		user2 = users.get(1);
		
		List<TaskGroup> groups = userMgmt.findAllGroups();
		group1 = groups.get(0);
		group2 = groups.get(1);
	}
	
	private Approval createTestApproval1() {
		Approval approval = createApproval("Test Approval");
		ApprovalStep step1 = addStep(approval, ApprovalStep.ALL_APPROVE);
		addUserApprover(step1, user1);
		
		ApprovalStep step2 = addStep(approval, ApprovalStep.ALL_APPROVE);
		addUserApprover(step2, user1);
		addGroupApprover(step2, group1);
		addGroupApprover(step2, group2);
		
		ApprovalStep step3 = addStep(approval, ApprovalStep.ANY_APPROVE);
		addUserApprover(step3, user1);
		addUserApprover(step3, user2);
		addGroupApprover(step3, group2);
		
		approval = approvalDao.saveApproval(approval);
		
		return approval;
	}
	
	private Approval createApproval(String name) {
		Approval approval = new Approval();
		approval.setName(name);
		approval.setDescription(name + " description");
		return approval;
	}
	
	private ApprovalStep addStep(Approval approval, String type) {
		ApprovalStep step = new ApprovalStep();
		step.setType(type);
		approval.addStep(step);
		return step;
	}
	
	private StepApprover addUserApprover(ApprovalStep step, TaskUser user) {
		StepApprover approver = new StepApprover();
		approver.setPrincipalId(user.getId());
		approver.setType(StepApprover.USER_TYPE);
		approver.setLabel(user.getDisplayName());
		step.addApprover(approver);
		return approver;
	}
	
	private StepApprover addGroupApprover(ApprovalStep step, TaskGroup group) {
		StepApprover approver = new StepApprover();
		approver.setPrincipalId(group.getId());
		approver.setType(StepApprover.GROUP_TYPE);
		approver.setLabel(group.getDisplayName());
		step.addApprover(approver);
		return approver;
	}
}
