package com.redhat.solutions.bpms.usermgmt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.redhat.solutions.bpms.usermgmt.TaskGroup;
import com.redhat.solutions.bpms.usermgmt.TaskUser;
import com.redhat.solutions.bpms.usermgmt.UserManagementService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext1.xml")
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
@Transactional
public class UserManagementServiceIT {
	
	private TaskUser bpmadmin;
	private TaskUser bpmuser;
	private TaskUser jowest;
	
	private TaskGroup bpmadmins;
	private TaskGroup bpmusers;
	
	private int groupsBeforeSize;
	private int usersBeforeSize;
	
	@Autowired
	private UserManagementService userMgmtService;
	
	@PersistenceContext
	private EntityManager em;
	
	@Before
	public void setup() {
		countPretestData();
	}
	
	private void countPretestData() {
		List<TaskUser> usersBefore = userMgmtService.findAllUsers();
		usersBeforeSize = usersBefore.size();
		List<TaskGroup> groupsBefore = userMgmtService.findAllGroups();
		groupsBeforeSize = groupsBefore.size();
	}
	
	private void constructTestGroups() {
		bpmadmins = new TaskGroup("bpmadmins", "BPM Administrators", "en-US", "bpmadmins@redhat.com");
		bpmusers = new TaskGroup("bpmusers", "BPM Users", "en-US", "bpmusers@redhat.com");
	}
	
	private void constructTestUsers() {
		bpmadmin = new TaskUser("bpmadmin", "BPM admin", "en-US", "bpmadmin@redhat.com");	
		bpmuser = new TaskUser("bpmuser", "BPM User", "en-US", "bpmuser@redhat.com");	
		jowest = new TaskUser("jowest", "Josh West", "en-US", "jowest@redhat.com");	
	}
	
	private void assignGroups() {
		bpmadmin.addGroup(bpmadmins);
		bpmuser.addGroup(bpmusers);
		jowest.addGroup(bpmadmins);
		jowest.addGroup(bpmusers);
	}
	
	private void constructAllTestData() {
		constructTestGroups();
		constructTestUsers();
		assignGroups();
	}
	
	private void saveGroups() {
		userMgmtService.saveGroup(bpmadmins);	
		userMgmtService.saveGroup(bpmusers);	
	}
	
	private void saveUsers() {
		userMgmtService.saveUser(bpmadmin);
		userMgmtService.saveUser(bpmuser);
		userMgmtService.saveUser(jowest);
	}
	
	private void saveAllTestData() {
		constructTestGroups();
		saveGroups();
		constructTestUsers();
		saveUsers();
		assignGroups();
		saveUsers();
	}
	
	@Test
	public void findAllUsersTest() {
		saveAllTestData();
		
		List<TaskUser> users = userMgmtService.findAllUsers();
		assertNotNull(users);
		assertEquals(usersBeforeSize + 3, users.size());
	}
	
	@Test
	public void saveUserTest() {
		constructAllTestData();
		userMgmtService.saveGroup(bpmusers);
		userMgmtService.saveUser(bpmuser);
		
		TaskUser user = userMgmtService.findUser(bpmuser.getId());
		assertNotNull(user);
		assertEquals(bpmuser.getId(), user.getId());
		assertEquals(bpmuser.getDisplayName(), user.getDisplayName());
		assertEquals(bpmuser.getLanguage(), user.getLanguage());
		assertEquals(bpmuser.getEmail(), user.getEmail());
		
		assertNotNull(user.getGroupAssignments());
		assertEquals(1, user.getGroupAssignments().size());	

		// Not expected to cascade
		List<TaskGroup> groups = userMgmtService.findAllGroups();
		assertEquals(groupsBeforeSize + 1, groups.size());
	}
	
	@Test
	public void findUsersByGroupTest() {
		saveAllTestData();
		
		List<TaskUser> admins = userMgmtService.findUsersByGroup("bpmadmins");
		assertNotNull(admins);
		assertEquals(2, admins.size());
	}
	
	@Test
	public void findUserTest() {
		constructAllTestData();
		userMgmtService.saveGroup(bpmadmins);
		userMgmtService.saveGroup(bpmusers);
		userMgmtService.saveUser(jowest);
		
		TaskUser user = userMgmtService.findUser(jowest.getId());
		assertNotNull(user);
		assertEquals(jowest.getDisplayName(), user.getDisplayName());
		assertEquals(jowest.getEmail(), user.getEmail());
		assertEquals(jowest.getLanguage(), user.getLanguage());
		assertEquals(jowest.getGroupAssignments().size(), user.getGroupAssignments().size());
	}
	
	@Test
	public void findAllGroupsTest() {
		constructAllTestData();
		userMgmtService.saveGroup(bpmadmins);
		userMgmtService.saveGroup(bpmusers);
		
		List<TaskGroup> groups = userMgmtService.findAllGroups();
		assertNotNull(groups);
		assertEquals(groupsBeforeSize + 2, groups.size());
	}

	@Test
	public void findGroupTest() {
		constructAllTestData();
		userMgmtService.saveGroup(bpmadmins);
		
		TaskGroup group = userMgmtService.findGroup(bpmadmins.getId());
		assertNotNull(group);
		assertEquals(group.getDisplayName(), bpmadmins.getDisplayName());
		assertEquals(group.getEmail(), bpmadmins.getEmail());
		assertEquals(group.getLanguage(), bpmadmins.getLanguage());
	}
	
	@Test
	public void removeUserTest() {
		saveAllTestData();
		
		userMgmtService.removeUser(jowest.getId());
		
		List<TaskUser> users = userMgmtService.findAllUsers();
		assertEquals(usersBeforeSize + 2, users.size());
		
		List<TaskGroup> groups = userMgmtService.findAllGroups();
		assertEquals(groupsBeforeSize + 2, groups.size());
	}
	
	@Test
	public void saveGroupTest() {
		constructAllTestData();
		userMgmtService.saveGroup(bpmadmins);
		
		TaskGroup group = userMgmtService.findGroup(bpmadmins.getId());
		assertNotNull(group);
		assertEquals(bpmadmins.getId(), group.getId());
		assertEquals(bpmadmins.getDisplayName(), group.getDisplayName());
		assertEquals(bpmadmins.getLanguage(), group.getLanguage());
		assertEquals(bpmadmins.getEmail(), group.getEmail());		
	}
	
	@Test
	public void removeGroupTest() {
		saveAllTestData();
		
		userMgmtService.removeGroup(bpmadmins.getId());
		
		List<TaskGroup> groups = userMgmtService.findAllGroups();
		assertEquals(groupsBeforeSize + 1, groups.size());
		
		// users should not be deleted
		List<TaskUser> users = userMgmtService.findAllUsers();
		assertEquals(usersBeforeSize + 3, users.size());
	}
}
