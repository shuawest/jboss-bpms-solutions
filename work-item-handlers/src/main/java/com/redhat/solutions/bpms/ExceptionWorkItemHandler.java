package com.redhat.solutions.bpms;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionWorkItemHandler implements WorkItemHandler {

	private static final Logger logger = LoggerFactory.getLogger(ExceptionWorkItemHandler.class);

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		try{
			logger.info("Executing work item: " + workItem.getName());
			manager.completeWorkItem(workItem.getId(), null);
			
		} catch(Exception e) {
	        logger.error("Error caught while executing work item", e);
		}
	}

	@Override
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        logger.info("Aborting work item: " + workItem.getName());
	}
	
}
